Product.destroy_all
ProductCategory.destroy_all
Restaurant.destroy_all
Category.destroy_all
User.destroy_all
Coupon.destroy_all

puts 'Criando Categorias'

path_image = 'public/images/categories/mexican.jpg'
c = Category.create(id: 1, title: 'mexicana')
c.image.attach(io: open(path_image), filename: 'mexican.jpg')

path_image = 'public/images/categories/italian.jpeg'
c = Category.create(id: 2, title: 'italiana')
c.image.attach(io: open(path_image), filename: 'italian.jpeg')

path_image = 'public/images/categories/japonese.jpeg'
c = Category.create(id: 3, title: 'japonesa')
c.image.attach(io: open(path_image), filename: 'japonese.jpeg')

path_image = 'public/images/categories/vegan.jpeg'
c = Category.create(id: 4, title: 'vegana')
c.image.attach(io: open(path_image), filename: 'vegan.jpeg')


path_image = 'public/images/categories/peruvian.jpg'
c = Category.create(id: 5, title: 'vegana')
c.image.attach(io: open(path_image), filename: 'peruana.jpg')


puts 'Cadastrando Restaurantes'


# Italian Restaurants
path_image = 'public/images/restaurants/luigi.png'
r = Restaurant.create!(
	id: 1,
	name: 'Luigi Pizzas',
	description: 'Sua pizza, do seu jeito.',
	status: 'open', delivery_tax: 5.50,
	state: 'RJ', city: 'Barra Mansa', street: 'Rua Eduardo Junqueira',
	number: '388', neighborhood: 'Centro', category_id: 2
	)
r.image.attach(io: open(path_image), filename: 'luigi.png')

promo = ProductCategory.create!(title: 'Promoções', show: 1, restaurant: r)
pz = ProductCategory.create!(title: 'Pizzas', show: 1, restaurant: r)
pzd = ProductCategory.create!(title: 'Pizzas Doces', show: 1, restaurant: r)
lt = ProductCategory.create!(title: 'Bebidas', show: 1, restaurant: r)
ad = ProductCategory.create!(title: 'Adicionais', show: 0, restaurant: r)
bd = ProductCategory.create!(title: 'Bordas', show: 0, restaurant: r)
cp = ProductCategory.create!(title: 'Extra', show: 0, restaurant: r)


###### - PROMOÇÃO DO DIA - ######


prod = Product.create!(name: 'LUIGI COMBO', description: 'PIZZA TRADICIONAL + BROTINHO DOCE + MANTIQUEIRA 2L', price:45, availability: 1, product_category: promo) 
prod.image.attach(io: open('public/images/products/COMBO.png'), filename: 'COMBO.png')

###### - PROMOÇÃO DO DIA - ######

prod = Product.create!(name: 'PROMOÇÃO DO DIA', description: 'CALABRESA', price:26, availability: 1, product_category: promo) 
prod.image.attach(io: open('public/images/products/CALABRESA.png'), filename: 'CALABRESA.png')


prod = Product.create!(name: 'PROMOÇÃO DO DIA', description: 'VEGETARIANA', price:26, availability: 1, product_category: promo) 
prod.image.attach(io: open('public/images/products/VEGETARIANA.png'), filename: 'VEGETARIANA.png')

prod = Product.create!(name: 'PROMOÇÃO DO DIA', description: 'QUATRO QUEIJOS', price:26, availability: 1, product_category: promo) 
prod.image.attach(io: open('public/images/products/QUATROQUEIJOS.png'), filename: 'QUATROQUEIJOS.png')


prod = Product.create!(name: 'PROMOÇÃO DO DIA', description: 'QUATRO QUEIJOS', price:26, availability: 1, product_category: promo) 
prod.image.attach(io: open('public/images/products/QUATROQUEIJOS.png'), filename: 'QUATROQUEIJOS.png')


prod = Product.create!(name: 'PROMOÇÃO DO DIA', description: 'PORTUGUESA', price:26, availability: 1, product_category: promo) 
prod.image.attach(io: open('public/images/products/PORTUGUESA.png'), filename: 'PORTUGUESA.png')

prod = Product.create!(name: 'PROMOÇÃO DO DIA', description: 'ABC', price:26, availability: 1, product_category: promo) 
prod.image.attach(io: open('public/images/products/ABC.png'), filename: 'ABC.png')


prod = Product.create!(name: 'PROMOÇÃO DO DIA', description: 'FRANGO COM CATUPIRY', price:26, availability: 1, product_category: promo) 
prod.image.attach(io: open('public/images/products/FRANGOCOMCATUPIRY.png'), filename: 'FRANGOCOMCATUPIRY.png')





###### - PIZZAS SALGADAS - ######

prod = Product.create!(name: 'MUSSARELA', description: 'Mussarela.', price:23, availability: 1, product_category: pz) 
prod.image.attach(io: open('public/images/products/MUSSARELA.png'), filename: 'MUSSARELA.png')


prod = Product.create!(name: 'CALABRESA', description: 'Mussarela, calabresa e cebola.', price:29, availability: 1, product_category: pz) 
prod.image.attach(io: open('public/images/products/CALABRESA.png'), filename: 'CALABRESA.png')


#prod = Product.create!(name: 'BMC', description: 'Mussarela, bacon, milho e cheddar', price:31, availability: 1, product_category: pz) 
#prod.image.attach(io: open('public/images/products/BMC.png'), filename: 'BMC.png')


prod = Product.create!(name: 'LOMBO COM CHEDDAR', description: 'Mussarela, lombo e cheddar.', price:32, availability: 1, product_category: pz) 
prod.image.attach(io: open('public/images/products/LOMBOCOMCHEDDAR.png'), filename: 'LOMBOCOMCHEDDAR.png')


prod = Product.create!(name: 'VEGETARIANA', description: 'Mussarela, tomate, pimentão, ovo, palmito e catupiry.', price:31, availability: 1, product_category: pz) 
prod.image.attach(io: open('public/images/products/VEGETARIANA.png'), filename: 'VEGETARIANA.png')

prod = Product.create!(name: 'FRANGO COM CATUPIRY', description: 'Mussarela, frango e catupiry. ', price:31, availability: 1, product_category: pz) 
prod.image.attach(io: open('public/images/products/FRANGOCOMCATUPIRY.png'), filename: 'FRANGOCOMCATUPIRY.png')


#prod = Product.create!(name: 'FRANGO COM CHEDDAR', description: 'Mussarela, frango e cheddar', price:30, availability: 1, product_category: pz) 
#prod.image.attach(io: open('public/images/products/FRANGOCOMCHEDDAR.png'), filename: 'FRANGOCOMCHEDDAR.png')


prod = Product.create!(name: 'ABC', description: 'Mussarela, alho, bacon e champignon', price:33, availability: 1, product_category: pz) 
prod.image.attach(io: open('public/images/products/ABC.png'), filename: 'ABC.png')

prod = Product.create!(name: 'PORTUGUESA', description: 'Mussarela, presunto, ovo, tomate, cebola, milho e azeitona.', price:34, availability: 1, product_category: pz) 
prod.image.attach(io: open('public/images/products/PORTUGUESA.png'), filename: 'PORTUGUESA.png')

prod = Product.create!(name: 'PEPPERONI', description: 'Mussarela e pepperoni.', price:31, availability: 1, product_category: pz) 
prod.image.attach(io: open('public/images/products/PEPPERONI.png'), filename: 'PEPPERONI.png')


prod = Product.create!(name: 'QUATRO QUEIJOS', description: 'Mussarela, parmesão, gorgonzola e catupiry.', price:34, availability: 1, product_category: pz) 
prod.image.attach(io: open('public/images/products/QUATROQUEIJOS.png'), filename: 'QUATROQUEIJOS.png')


prod = Product.create!(name: 'CINCO QUEIJOS', description: 'Mussarela, parmesão, gorgonzola, catupiry e cheddar', price:37, availability: 1, product_category: pz) 
prod.image.attach(io: open('public/images/products/CINCOQUEIJOS.png'), filename: 'CINCOQUEIJOS.png')

#prod = Product.create!(name: 'VEGANA', description: 'Queijo vegano, tomate, palmito, alho e champignon', price:30, product_category: pz) 
#prod.image.attach(io: File.open('public/images/products/VEGANA.png'), filename: 'VEGANA.png')


###### - PIZZAS DOCES - ######

prod = Product.create!(name: 'ROMEU E JULIETA', description: 'Mussarela e goiabada', price:10, availability: 1, product_category: pzd) 
prod.image.attach(io: open('public/images/products/SMALLBLANK.png'), filename: 'SMALLBLANK.png')


prod = Product.create!(name: 'BRIGADEIRO', description: 'Chocolate ao leite e granulado', price:10, availability: 1, product_category: pzd) 
prod.image.attach(io: open('public/images/products/SMALLBLANK.png'), filename: 'SMALLBLANK.png')


prod = Product.create!(name: 'BRIGADEIRO BRANCO', description: 'Chocolate branco e granulado', price:10, availability: 1, product_category: pzd) 
prod.image.attach(io: open('public/images/products/SMALLBLANK.png'), filename: 'SMALLBLANK.png')

prod = Product.create!(name: 'PRESTÍGIO', description: 'Chocolate ao leite e côco ralado', price:10, availability: 1, product_category: pzd) 
prod.image.attach(io: open('public/images/products/SMALLBLANK.png'), filename: 'SMALLBLANK.png')

prod = Product.create!(name: 'NUTELLA', description: 'Creme de avelã', price:12, availability: 1, product_category: pzd) 
prod.image.attach(io: open('public/images/products/SMALLBLANK.png'), filename: 'SMALLBLANK.png')


prod = Product.create!(name: 'CHOCO DUPLO', description: 'Chocolate ao leite e chocolate branco', price:12, availability: 1, product_category: pzd) 
prod.image.attach(io: open('public/images/products/SMALLBLANK.png'), filename: 'SMALLBLANK.png')


###### - REFRIGERANTES - ######
prod = Product.create!(name: 'COCA-COLA 2L', price: 10, description: '', availability: 1,  product_category: lt)
prod.image.attach(io: open('public/images/products/COCA2L.png'), filename: 'COCA2L.png')

prod = Product.create!(name: 'MANTIQUEIRA 2L', price: 6, description: '', availability: 1,  product_category: lt)
prod.image.attach(io: open('public/images/products/MANTIQUEIRA2L.png'), filename: 'MANTIQUEIRA2L.png')


#prod.image.attach(io: open('public/images/products/BLANK_ICON.png'), filename: 'BLANK_ICON.png')


prod = Product.create!(name: 'Cheddar', price:3, description: '', availability: 1,  product_category: ad) 
prod.image.attach(io: open('public/images/products/BLANK_ICON.png'), filename: 'BLANK_ICON.png')
prod = Product.create!(name: 'Catupiry', price:3, description: '', availability: 1,  product_category: ad) 
prod.image.attach(io: open('public/images/products/BLANK_ICON.png'), filename: 'BLANK_ICON.png')
prod = Product.create!(name: 'Gorgonzola', price:4, description: '', availability: 1,  product_category: ad) 
prod.image.attach(io: open('public/images/products/BLANK_ICON.png'), filename: 'BLANK_ICON.png')
prod = Product.create!(name: 'Parmesão', price:4,description: '', availability: 1,  product_category: ad) 
prod.image.attach(io: open('public/images/products/BLANK_ICON.png'), filename: 'BLANK_ICON.png')

prod = Product.create!(name: 'Bacon', price:5,description: '', availability: 1,  product_category: ad) 
prod.image.attach(io: open('public/images/products/BLANK_ICON.png'), filename: 'BLANK_ICON.png')
prod = Product.create!(name: 'Calabresa', price:5,description: '', availability: 1,  product_category: ad) 
prod.image.attach(io: open('public/images/products/BLANK_ICON.png'), filename: 'BLANK_ICON.png')
prod = Product.create!(name: 'Frango', price:5,description: '', availability: 1,  product_category: ad) 
prod.image.attach(io: open('public/images/products/BLANK_ICON.png'), filename: 'BLANK_ICON.png')
prod = Product.create!(name: 'Presunto', price:6,description: '', availability: 1,  product_category: ad) 
prod.image.attach(io: open('public/images/products/BLANK_ICON.png'), filename: 'BLANK_ICON.png')
prod = Product.create!(name: 'Lombo', price:6,description: '', availability: 1,  product_category: ad) 
prod.image.attach(io: open('public/images/products/BLANK_ICON.png'), filename: 'BLANK_ICON.png')
prod = Product.create!(name: 'Pepperoni', price:8,description: '', availability: 1,  product_category: ad) 
prod.image.attach(io: open('public/images/products/BLANK_ICON.png'), filename: 'BLANK_ICON.png')


prod = Product.create!(name: 'Tomate', price:1,description: '', availability: 1,  product_category: ad) 
prod.image.attach(io: open('public/images/products/BLANK_ICON.png'), filename: 'BLANK_ICON.png')
prod = Product.create!(name: 'Azeitona', price:1,description: '', availability: 1,  product_category: ad) 
prod.image.attach(io: open('public/images/products/BLANK_ICON.png'), filename: 'BLANK_ICON.png')
prod = Product.create!(name: 'Cebola', price:1,description: '', availability: 1,  product_category: ad) 
prod.image.attach(io: open('public/images/products/BLANK_ICON.png'), filename: 'BLANK_ICON.png')
prod = Product.create!(name: 'Milho', price:1,description: '', availability: 1,  product_category: ad) 
prod.image.attach(io: open('public/images/products/BLANK_ICON.png'), filename: 'BLANK_ICON.png')
prod = Product.create!(name: 'Ovo', price:1,description: '', availability: 1,  product_category: ad) 
prod.image.attach(io: open('public/images/products/BLANK_ICON.png'), filename: 'BLANK_ICON.png')
prod = Product.create!(name: 'Alho', price:2,description: '', availability: 1,  product_category: ad) 
prod.image.attach(io: open('public/images/products/BLANK_ICON.png'), filename: 'BLANK_ICON.png')
prod = Product.create!(name: 'Palmito', price:2,description: '', availability: 1,  product_category: ad) 
prod.image.attach(io: open('public/images/products/BLANK_ICON.png'), filename: 'BLANK_ICON.png')
prod = Product.create!(name: 'Champignon', price:3, description: '', availability: 1,  product_category: ad) 
prod.image.attach(io: open('public/images/products/BLANK_ICON.png'), filename: 'BLANK_ICON.png')


prod = Product.create!(name: 'SEM RECHEIO', price:0, description: '', availability: 1,  product_category: bd) 
prod.image.attach(io: open('public/images/products/BLANK_ICON.png'), filename: 'BLANK_ICON.png')
#prod = Product.create!(name: 'PROVOLONE', price:4, description: 'Borda recheada sabor Provolone.', availability: 1,  product_category: bd) 
#prod.image.attach(io: open('public/images/products/BLANK_ICON.png'), filename: 'BLANK_ICON.png')
prod = Product.create!(name: 'CHEDDAR', price:4, description: 'Borda recheada sabor Cheddar.', availability: 1,  product_category: bd) 
prod.image.attach(io: open('public/images/products/BLANK_ICON.png'), filename: 'BLANK_ICON.png')
prod = Product.create!(name: 'CATUPIRY', price:4, description: 'Borda recheada sabor Catupiry.', availability: 1,  product_category: bd) 
prod.image.attach(io: open('public/images/products/BLANK_ICON.png'), filename: 'BLANK_ICON.png')

prod = Product.create!(name: 'PRIMEIRAVEZ', price:-10, description: '', availability: 1,  product_category: cp) 
prod.image.attach(io: open('public/images/products/BLANK_ICON.png'), filename: 'BLANK_ICON.png')

coup = Coupon.create!(name: 'PRIMEIRAVEZ', kind:1, value:10, min_value: 22, max_uses: 0, expiration_date:"2019-11-15")
coup = Coupon.create!(name: 'MAE10', kind:2, value:10, multiplier: 0.9, min_value: 22, max_uses: 0, expiration_date:"2020-05-11")

admin = User.new
admin.email = 'irvingsslima@gmail.com'
admin.password = '123ioio'
admin.password_confirmation = '123ioio'
admin.role = 1
admin.save

admin2 = User.new
admin2.email = 'iohann@luigi.com.br'
admin2.password = '123ioio'
admin2.password_confirmation = '123ioio'
admin2.role = 1
admin2.save

user = User.new
user.email = 'irving_bm@hotmail.com'
user.password = '123ioio'
user.password_confirmation = '123ioio'
user.save