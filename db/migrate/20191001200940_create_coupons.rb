class CreateCoupons < ActiveRecord::Migration[5.2]
  def change
    create_table :coupons do |t|
      t.string :name
      t.integer :kind, default:1
      t.integer :value
      t.float :multiplier, default:1.0
      t.integer :discount, default:0
      t.integer :min_value, default:0
      t.boolean :works, default:true
      t.integer :max_uses, default:0
      t.timestamp :expiration_date

      t.timestamps
    end
  end
end
