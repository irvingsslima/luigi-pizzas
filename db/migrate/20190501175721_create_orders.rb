class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.string :name
      t.string :phone_number
      t.float :value_received, default: 0    
      t.float :total_value    
      t.float :total_value_discount    
      t.float :change, default: 0
      t.string :address
      t.integer :delivery_tax, default: 99
      t.string :coupon_name, default: ""
      t.integer :coupon, default: 0
      t.integer :status, default: 0
      t.references :restaurant, foreign_key: true

      t.timestamps
    end
  end
end
