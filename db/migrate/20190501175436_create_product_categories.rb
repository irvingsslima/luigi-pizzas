class CreateProductCategories < ActiveRecord::Migration[5.2]
  def change
    create_table :product_categories do |t|
      t.string :title      
      t.boolean :show, default: false
      t.references :restaurant, foreign_key: true

      t.timestamps
    end
  end
end
