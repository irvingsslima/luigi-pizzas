class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :name
      t.text :description
      t.float :price
      t.integer :availability, default: 1
      t.integer :product_category
      t.references :product_category, foreign_key: true

      t.timestamps
    end
  end
end
