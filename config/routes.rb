Rails.application.routes.draw do
  resources :coupons
  resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  ### RESTAURANTS ###
  get '/api/restaurants', to: 'restaurants#index' 
  get '/api/restaurants/search', to: 'restaurants#search'  
  get '/api/restaurants/:id', to: 'restaurants#show'
  put '/api/restaurants/:id', to: 'products#update'

  ### PRODUCTS ###
  post '/api/products', to: 'products#create'
  get '/api/products', to: 'products#index'  
  get '/api/products/search', to: 'products#search' 
  put '/api/products/:id', to: 'products#update'
  #get '/api/products/:id', to: 'products#show'

  ### ORDERS ###
  post '/api/orders', to: 'orders#create'
  get '/api/orders', to: 'admin#index'
  get '/api/orders/:id', to: 'orders#show'
  put '/api/orders/:id', to: 'orders#update'

  ### COUPONS ###
  post '/api/coupons', to: 'coupons#create'
  get '/api/coupons', to: 'coupons#index'
  get '/api/coupons/:id', to: 'coupons#show'
  put '/api/coupons/:id', to: 'coupons#update'  
  

  get '/api/categories', to: 'categories#index'  
  get '/api/admin', to: 'admin#show'

  mount ActionCable.server => '/cable'
  

  ### USERS ###
  scope '/api' do
    resources :users
    post 'user_token' => 'user_token#create'
    post '/users' => 'users#create'    
    post '/find_user' => 'users#find'
  end


end


#searchRestaurants: (search) => url.get(`/api/restaurants/search?q=${search}`),
  
#searchProducts: (search) => url.get(`/api/products/search?q=${search}`),      