import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Radio, Input, Column, Title, Button, Field, Image, Control, Box, Label, Checkbox} from "rbx";
import { bindActionCreators } from 'redux';
import { hideModal, showModal } from "../../actions/modal";
import { addOrderItem } from "../../actions/new_order";

//onlyPizzas={this.state.products.filter((product) => product.status === "waiting")}/>
let twoFlavor = 1;
let bordaCheck = [1,0,0,0];

class AddProductForm extends Component {  
  constructor(props) {
    super(props);
    
    this.state = {
      quantity:  1,
      product_id: props.product.id,
      comment: '',
      products: props.products,      
      second_flavor_id: '',
      combo_salg: '',
      combo_doce: '',
      borda: '',
      complementos: []
    }
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleFlavorChange = this.handleFlavorChange.bind(this);

  }

  handleInputChange(event) {


    const target = event.target;
    const value = target.value;
    const name = target.name;

    if(name==="complementos")
    this.setState(prevState => ({ complementos: [...prevState.complementos, value]

      
    
  
  }));
    else
    this.setState({
      [name]: value
    });
  }

  handleSubmit(event) {


  

      //this.setState(prevState => ({ comment: [...prevState.comment, this.state.combo_salg + " " + this.state.combo_doce]

      
    
  
      //}));
  
  /*
      this.setState({
        //comment: this.state.combo_salg + " " + this.state.combo_doce
      });
      */

    
    
    event.preventDefault();
    
    let sentence = "";

      if(this.props.product.product_category.title ===  "Promoções" && this.props.product.id === 1)
    sentence = this.state.combo_salg + " + " + this.state.combo_doce + " + MANTIQUEIRA 2L " ;

    //TODO: Gambiarra do caramba, resolver logo
    if(this.state.second_flavor_id.length > 0)
    {
      console.log("Meia: "+JSON.stringify(this.props.restaurant.product_categories[1].products[this.state.second_flavor_id]))
      this.props.addOrderItem(this.props.restaurant, this.props.product, 0.5, this.state.comment)      
      this.props.addOrderItem(this.props.restaurant, this.props.restaurant.product_categories[1].products[this.state.second_flavor_id], 0.5, this.state.comment)
    }
    else
    {

    this.props.addOrderItem(this.props.restaurant, this.props.product, this.state.quantity, sentence + this.state.comment)
    }

    this.state.complementos.map((comp_id) => {
    
       ;
      comp_id = comp_id -this.props.restaurant.product_categories[4].products[0].id;//TODO: SUBSTITUIR PELO TITLE (COMPLEMENTOS)
    
      return (
        this.props.addOrderItem(this.props.restaurant, this.props.restaurant.product_categories[4].products[comp_id], 1, this.state.comment)
      )
      });

      if(this.state.borda > 0){
        let indexBorda = this.state.borda - this.props.restaurant.product_categories[5].products[0].id;
        this.props.addOrderItem(this.props.restaurant, this.props.restaurant.product_categories[5].products[indexBorda], 1, this.state.comment)
      }

      



    this.props.hideModal('ADD_PRODUCT')    
    this.props.showModal('GO2CART_MODAL')
    this.forceUpdate()
  };

  
  handleFlavorChange(){
    twoFlavor = !twoFlavor;
    
  }
  

  render(){
    return(
      <Fragment>
        <Column.Group >
          <Column >

          {(this.props.product.product_category.title ===  "Promoções" && this.props.product.id === 1 )&&
        <Fragment> 
        <Label className="warning-box">
        ATENÇÃO: As pizzas do combo são de apenas um sabor!
        </Label>
        &nbsp;
      </Fragment>
      }
          
          
          {(this.props.product.product_category.title ===  "Promoções" && this.props.product.id > 1 )&&
        <Fragment> 
        <Label className="warning-box">
        ATENÇÃO: Para a pizza do dia (promoção) não é permitido 2 sabores
        </Label>
        &nbsp;
      </Fragment>
      }

            <Title size={6} className="has-text-custom-black-darker">
              {this.props.product.name}
            </Title>
            <Title size={6} subtitle className="has-text-custom-black-darker">
              {this.props.product.description}
            </Title>
  
           

    
            {this.state.second_flavor_id.length > 0 && 
              <Fragment> 
                &nbsp;
                <Title size={6} className="has-text-custom-black-darker">
              {this.props.restaurant.product_categories[1].products[this.state.second_flavor_id].name}
            </Title>
            <Title size={6} subtitle className="has-text-custom-black-darker">
              {this.props.restaurant.product_categories[1].products[this.state.second_flavor_id].description}
            </Title>
            </Fragment>
            }

            

      {this.props.product.product_category.title !== "Pizzas Doces" && 
          <Image.Container>
            <div className="cut-image">
            <img src={this.props.product.image_url} className="under" alt={this.props.product.name}/>
            {this.state.second_flavor_id.length > 0 && <img src={this.props.restaurant.product_categories[1].products[this.state.second_flavor_id].image_url }  alt={this.props.restaurant.product_categories[1].products[this.state.second_flavor_id].name} className="over"/>
            }
            </div>
          </Image.Container>
  }
          
            <form onSubmit={this.handleSubmit}>
                {this.props.product.product_category.title === "Pizzas Salgadas (Maracanã)"? //Show second flavor and options only to products in "pizza" category
          <Fragment>  



{this.props.product.product_category.title === "Pizzas Salgadas (Maracanã)" && //Excle promo of the dat
            <Box>
               <Label>Para pizza meio a meio escolha um segundo sabor:</Label>
                <Control>


                 

                      <select name="second_flavor_id" value={this.state.value} onChange={this.handleInputChange}>
                        <option value=""></option>
                        {this.props.restaurant.product_categories[1].products.map((product, i) => {                          
                        return (
                          product.availability > 0 && product.id !== this.props.product.id &&
                        <option  value={product.id - this.props.restaurant.product_categories[1].products[0].id} key={i}>{product.name}</option>
                        )
                        })};

                      </select>



                </Control>           

              </Box>



                      }


              <Field>
                <Box>
                  
                  <Label>Borda recheada:</Label>
                  
                <span className="short_dashed_box">R$</span>
                      <br></br>
{this.props.restaurant.product_categories[5].products.map((product, i) => {
  return (product.availability > 0 &&
                      <p  key={i}>
                        <label>
                      <Radio 
                      className="borda"
                      name="borda"                      
                      defaultChecked = {bordaCheck[i]}
                      value={product.id}
                      onChange={this.handleInputChange}
                      />
                       {" "+product.name} <span className="short_dashed_box">+{product.price}</span>
                       </label>
                       </p>
                      )
  

                  
                    })}
                      </Box>
              </Field>



{/*
              <Field>
                <Box>
                  <Label>Complementos:</Label>
                <span className="short_dashed_box">R$</span>
                      <br></br>
                  {this.props.restaurant.product_categories[4].products.map((product, i) => {
  return (
    product.availability > 0 &&
                  <p key={i}>
                    <label>
                    <Checkbox 
                      className="complementos"
                      name="complementos"                      
                      value={product.id}
                      defaultChecked= {false}
                      onChange={this.handleInputChange}
                      />
                      {" "+product.name}  <span className="short_dashed_box">+{product.price}</span>
                      </label>
                      </p>
)
                    })}
                      </Box>
              </Field>


                  */
                  }

{
/*
              <Field>
                  <Label>Alguma observação?</Label>
                  <Control>
                    <Input 
                      type="text" 
                      name="comment"
                      placeholder="Sem cebola, sem pimentão..."
                      value={this.state.comment}
                      onChange={this.handleInputChange}
                      />
                </Control>
              </Field>
              */
}
          
              <br/>
</Fragment>
:

  (this.props.product.product_category.title ===  "Promoções" && this.props.product.id === 1 )?
<Fragment>
  
  <p></p>
  <Box>
               <Label>Escolha a pizza tradicional:</Label>
                <Control>


                 

                      <select name="combo_salg" value={this.state.value} onChange={this.handleInputChange} required>
                        <option value=""></option>
                        {this.props.restaurant.product_categories[1].products.map((product, i) => {                          
                        return (
                          product.availability > 0 && 
                        <option  value={product.name} key={i}>{product.name}</option>
                        )
                        })};

                      </select>



                </Control>           

              </Box>
              <p></p>
              <Box>
               <Label>Escolha a pizza doce:</Label>
                <Control>


                 

                      <select name="combo_doce" value={this.state.value} onChange={this.handleInputChange} required>
                        <option value=""></option>
                        {this.props.restaurant.product_categories[3].products.map((product, i) => {                          
                        return (
                          product.availability > 0 && 
                        <option  value={product.name} key={i}>{product.name}</option>
                        )
                        })};

                      </select>



                </Control>           

              </Box>
</Fragment>
:
<Fragment>
<Field>
  <p></p>
                  <Label>Quantidade:</Label>
                  <Control>
                    <Input 
                      min="1" 
                      max="5"
                      size="big"
                      type="number" 
                      name="quantity"
                      placeholder={1}
                      value={this.state.quantity}
                      onChange={this.handleInputChange}
                      />
                </Control>
              </Field>
</Fragment>
            }

            {this.props.product.product_category.title !== "Bebidas" && 

<Field>
                  <Label>Alguma observação?</Label>
                  <Control>
                    <Input 
                      type="text" 
                      name="comment"
                      placeholder="Sem cebola, sem pimentão..."
                      value={this.state.comment}
                      onChange={this.handleInputChange}
                      />
                </Control>
              </Field>
              }

<Field kind="group" align="centered" id="modal-footer">
                    <Column.Group>
                    <Column>
                              <Button onClick={() =>  this.props.hideModal('ADD_PRODUCT')   }  size="medium" color="custom-orange">
                                <span className="has-text-white">Voltar</span> 
                                </Button>  
                                </Column>
                                <Column>                 
                              <Button size="medium" color="default-green">
                                <span className="has-text-white">Adicionar produto</span>
                              </Button>
                              </Column>
            
                              </Column.Group>
                          </Field>
            </form>
            <div id="cover-spin"></div>
          </Column>
        </Column.Group>
      </Fragment>
    )
  }

}

const mapStateToProps = store => ({
  modal: store.modalState.modal,
  order: store.newOrderState.order,
  products: store.productsState.products
});

const mapDispatchToProps = dispatch => bindActionCreators({ hideModal, showModal, addOrderItem }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AddProductForm);