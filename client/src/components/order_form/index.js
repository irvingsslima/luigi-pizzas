import React, { Fragment, Component } from 'react';
import {  Radio, Box, Column, Title, Input, Field, Button, Control, Label, Image } from "rbx";
import { connect } from 'react-redux';
import api from "../../services/api";
import history from '../../history';
import { bindActionCreators } from "redux";
import { removeAllOrderItem } from "../../actions/new_order";
import money from '../../assets/images/money.png';
import pos from '../../assets/images/pos.png';
import Order from "../../components/order";
import Loader from 'react-loader-spinner'
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import axios from 'axios'



var validAddress = false;
var taxLimit = 7;

class OrderForm extends Component {

 

  
  async consultaCEP(args) {
    let arg = Object.assign({}, args)

    if ('cep' in arg === false) {
      throw new Error('You need to inform a CEP ex: { cep: 00000000 }');
    }

    //return new Promise((resolve, reject) => {
      const cepUrl = 'https://viacep.com.br/ws/{CEP}/json';
      let url = cepUrl.replace('{CEP}', arg.cep.replace('-', ''));

      return axios.get(url)
      /*

      request(url, (error, resp, body) => {
        if (error) {
          return reject(error);
        }

        try {
          return resolve(JSON.parse(body))

        } catch (e) {
          return reject({
            Erro: 404,
            MsgErro: 'Cep não encontrado'
          });
        }
      });

      */
   // });
  }
  
  
  constructor(props) {
    super(props);

    this.state = {
      name:  '',
      cep:   '',
      phone_number: '',
      number: '',
      bairro: '',
      rua: '',
      referencia: '',
      restaurant_id: 1,
      delivery_tax: -1,
      payment_method: 1,
      change: '',
      screen: 0,
      totalOrder: 0,
      coupon: 1,
      couponName: '',
      waitOrder: false,
      paymentScreen: false
    }
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);

    
  this.props.address.city = "Barra Mansa";
  this.props.address.state = "RJ";
  
  this.props.order.delivery_tax = this.state.delivery_tax
  }





/*

  deliveryCalc(origem, destino){
    //let arg = Object.assign({}, args)

    return new Promise((resolve, reject) => {
      const cepUrl = 'https://cors-anywhere.herokuapp.com/https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins={origem}&destinations={destino}&key={key}';
      let url = cepUrl.replace('{origem}', origem);
      url = url.replace('{destino}', destino);
      url = url.replace('{key}', google_key);

      
      validAddress = false;

      request(url, (error, resp, body) => {
        if (error) {
          return reject(error);
        }

        try {
          validAddress = true;
          return resolve(JSON.parse(body))

        } catch (e) {
          return reject({
            Erro: 404,
            MsgErro: 'Cep não encontrado'
          });
        }
      });
    });
  }
  */

  handleInputChange(event) {


    const target = event.target;
    const value = target.value;
    const name = target.name;


    this.setState({
      [name]: value
    });


    if(name === "payment_method" && value === "2"){
      console.log("Setando o troco pra -13");
      this.setState({
        change: -13
      });
    }


    this.forceUpdate();

    console.log(name+": "+value);
    

    if(name === "cep" && value.length > 7){
      this.handleCEP();
    }

  }
  
  removeAllItems = () => {
    this.props.removeAllOrderItem();
  }

  async handleSubmit(event) {
    event.preventDefault();

    
    
    this.props.address.name = this.state.name;
    this.props.address.street = this.state.rua;
    this.props.address.tel = this.state.phone_number;
    this.props.address.number  = this.state.number;
    this.props.address.reference  = this.state.bairro;
    this.props.address.extra  = this.state.referencia;
    

    if(this.state.paymentScreen){
      if(this.payment_method === 2){
        this.setState({
          change: -13
        })

      }
      this.setState({
        waitOrder: true
      })
     api.createOrder(this.state, this.props.order, this.props.address).then((response) => {
        let id = response.data.order.id 
  
        console.log("Order Products: " + JSON.stringify(this.props.order))
        console.log("STATE: " + JSON.stringify(this.state))
        console.log("Awaiting")
        console.log(response)

        if(response.status === 201){       
        console.log(response)    
        history.push(`/orders/${id}`)      
        this.removeAllItems()

        this.setState({          
        paymentScreen:false
        })
        }
        else
        console.log("Erro! "+response.status, response.error)
      }
      
      
      )



    }
    else{

      console.log("Changing to payment screen")
          
      this.setState({        
    paymentScreen: !this.state.paymentScreen
      })

    this.forceUpdate()      

    
  }

  }
  


  handlePayment(){
        
    this.setState({        
      paymentScreen: false
        })
    this.forceUpdate()
  }
/*
handleDelivery(event){
     
  console.log(this.props);

  this.deliveryCalc('27310000', this.props.address.cep).then(result => {

    this.setState({


      delivery_tax: (Math.round(result.rows[0].elements[0].distance.value/1000))



    })
    this.props.order.delivery_tax = this.state.delivery_tax    
    console.log(this.state.delivery_tax);
  
    
  })
}
*/
  handleCEP(event) {     
    this.consultaCEP({ cep: this.state.cep })    
  .then(result => {

    
    console.log(result); 
    
    this.setState({rua: result.data.logradouro});
    this.props.address.street = result.data.logradouro;
    this.props.address.cep = this.state.cep;
    this.props.address.state  = result.data.uf;
    this.props.address.city  = result.data.localidade;
    this.setState({bairro : result.data.bairro});
    this.props.address.reference = result.data.bairro;

    document.getElementById("name_input").focus();
    
    console.log(result);   
      this.forceUpdate();
    if(result.data.uf === "RJ")
      validAddress = true;
      else{
        validAddress = false;
        this.forceUpdate()
        //this.setState({delivery_tax: -1})
      }
  })
  .catch(error => {
    console.log(error);
  })

  }


  callbackFunction = (deliveryTax, cep_value, total) => {
    this.setState({screen: 1,
                    delivery_tax: deliveryTax,
                    cep: cep_value,
                    totalOrder: total
    })
};



  render() {
if(this.state.screen === 1){
    return (
<Fragment>
      {this.state.paymentScreen === false  ?
      <Column.Group>
      
      {validAddress ? "" : this.handleCEP()}
        <Column size={10} offset={1}>
            <br></br>
            <br></br>
                   
            
            <Column.Group>
            
              <Column size={10} offset={1}>
              <Title size={5} className="has-text-custom-green-darker">Informações de Entrega</Title>
                <form onSubmit={this.handleSubmit}>
                  <Field>
                    <Label>Nome</Label>
                    <Control>
                      <Input  id="name_input"
                        type="text" 
                        placeholder=""
                        name="name"
                        value={this.state.name}
                        onChange={this.handleInputChange}
                        required
                        />
                    </Control>
                  </Field>

                  <Field>
                    <Label>CEP</Label>
                    <Control>
                      <Input id='CEP_input'
                        type="number" 
                        placeholder='27345000'
                        name="cep"
                        value={this.state.cep}
                        onChange={this.handleInputChange}                         
                        disabled={true}      
                        />
                    </Control>
                  </Field>


              <Field>
                    <Label>Rua</Label>
                    <Control>
                      <Input 
                        type="text" 
                        placeholder=""
                        name="rua"
                        value={this.state.rua}
                        onChange={this.handleInputChange}
                        
                        disabled={true}
                        required
                        />
                    </Control>
                  </Field>

                  <Field>
                    <Label>Bairro</Label>
                    <Control>
                      <Input 
                        type="text" 
                        placeholder=""
                        name="bairro"
                        value={this.state.bairro}
                        onChange={this.handleInputChange}
                        disabled={true}
                        required
                        />
                    </Control>
                  </Field>


                  <Field>
                    <Label>Número</Label>
                    <Control>
                      <Input id='number_input'
                        type="number" 
                        placeholder="100"
                        name="number"
                        value={this.state.number}                        
                        onChange={this.handleInputChange}
                        required
                        />
                    </Control>
                  </Field>

                  

                  <Field>
                    <Label>Referência/Observação</Label>
                    <Control>
                      <Input 
                        type="text" 
                        placeholder=""
                        name="referencia"
                        value={this.state.referencia}
                        onChange={this.handleInputChange}                        
                        />
                    </Control>
                  </Field>


                  <Field>
                    <Label>Telefone</Label>
                    <Control>
                      <Input 
                        type="tel" 
                        placeholder='(24) 99876-5432'
                        name="phone_number"
                        value={this.state.phone_number}
                        onChange={this.handleInputChange}
                        required
                      />
                    </Control>
                  </Field>
                  <Field>
                    <br/>
                    <Title size={6} className="has-text-custom-gray-darker">
                      Endereço de entrega
                    </Title>
                    {validAddress ?
                    
                    <Fragment>
                    <p>
                      {this.props.address.street}, {this.state.number} ({this.state.referencia})
                      </p>
                      <p>
                      {this.props.address.reference}, {this.props.address.city} - {this.props.address.state}
                    </p> 
                       
                        
                        {this.state.delivery_tax < 1 ? 
                          <div className="dashed_box">Entrega grátis!</div> :                             
                               this.state.delivery_tax > taxLimit ? //Se o valor do frete é superior ao estipulado
                                <div className="dashed_box_red">Infelizmente não entregamos no seu endereço.</div>:
                                  <div className="dashed_box">{"Taxa de entrega: R$ "+this.state.delivery_tax.toFixed(2)}</div>
                             
                            
                            /*
                            :<div className="dashed_box_red">Infelizmente não entregamos no seu endereço.</div>}
                      


                      <Fragment>{this.state.delivery_tax < taxLimit ? 
                      </Fragment> 
                      */
                    
                      
                      
                      
                      }
                      
                        
                        </Fragment> 
                        :
                        <Fragment>
                        {<div className="dashed_box_red">O endereço está correto?</div> }
                        
                        </Fragment> 

                        }
                  </Field>

                  <br/>
                  {this.props.order.length > 0 && 
                    <Field kind="group" align="centered">
                    <Column.Group>
                    <Column>
                              <Button onClick={() => history.push('/') }  size="medium" color="custom-orange">
                                <span className="has-text-white">Voltar</span> 
                                </Button>  
                                </Column>
                                <Column>                 
                              {this.state.delivery_tax > -1 && <Button size="medium" color="default-green">
                                <span className="has-text-white">Pagamento</span>
                              </Button>
                              }
                              </Column>
            
                              </Column.Group>
                          </Field>
                  }
                </form>
              </Column>
            </Column.Group>
            
        </Column>
      </Column.Group>:




this.state.waitOrder ? <Loader
  type="Oval"
  color="green"
  height={100}
  width={100}
/>:

<Column.Group>

      
<Column size={10} offset={1}>
    <br></br>
    <br></br> 
    
    <Column.Group>
    
      <Column size={10} offset={1}>
      <Title size={5} className="has-text-custom-green-darker">Forma de pagamento</Title>
        <form onSubmit={this.handleSubmit}>

        <Title size={6} className="has-text-custom-green-darker">{`Total: ${this.state.totalOrder
        
        
        
        }`}</Title>
          <Field required>
            <Control>
            
            <label> 
            <Box>
            <p></p>
            <Image.Container size={48}><img src={money} alt="Pagamento em dinheiro"/></Image.Container>
      
                      <Radio
                      className="payment_method"
                      name="payment_method"                      
                      defaultChecked = {true}
                      value={1}
                      onChange={this.handleInputChange}
                      />
                       Dinheiro
                      
                      </Box>
                      </label>
                      <label> 
                      <Box>

                      <p></p>
                       
                      <Image.Container size={48}><img src={pos} alt="Pagamento em cartão"/></Image.Container>
                      <Radio
                      className="payment_method"
                      name="payment_method"                      
                      defaultChecked = {false}
                      value={2}
                      onChange={this.handleInputChange}
                      />
                       Crédito/Débito
                       
                      </Box>
                      </label>
            </Control>
          </Field>
{this.state.payment_method===1 && 
          <Field>
            <Label>Precisa de troco para qual valor?</Label>
            <Control>
              <Input id='change'
                type="number" 
                //placeholder={50}
                name="change"
                value={this.state.change}
                onChange={this.handleInputChange}       
                />
            </Control>

              {console.log("Change: "+this.state.change+" and totalOrder: "+this.state.totalOrder)}

            {this.state.change - this.state.totalOrder > 0 && <div><Title size={6} className="has-text-custom-gray-darker">
              <br/>
              <hr/>
              <br/>
          {`O motoboy levará R$${(this.state.change - this.state.totalOrder).toFixed(2)}.`}
              <br/>
        </Title>
          </div>}

        
</Field>
}

{this.state.payment_method===2 && 
  <Field>
    <Label>Cartão</Label>
    <Control>

    <Title size={6} className="has-text-custom-gray-darker">
                      O motoboy levará a máquina de cartão até você.
                    </Title>
    </Control>
  </Field>
}

          <Field kind="group" align="centered">
                    <Column.Group>
                    <Column>
                              <Button onClick={() => this.handlePayment() }  size="medium" color="custom-orange">
                                <span className="has-text-white">Voltar</span> 
                                </Button>  
                                </Column>
                                <Column>                 
                              <Button type="button" size="medium" color="default-green" onClick={(event) => this.handleSubmit(event)}>
                                <span className="has-text-white">Concluir</span>
                              </Button>
                              </Column>
            
                              </Column.Group>
                          </Field>
          </form>
          
        </Column>
      </Column.Group>
      
        </Column>
      </Column.Group>

                }

</Fragment>

    )
              }
              else
              {
                return(<Order parentCallback = {this.callbackFunction}/>)
              }
  } 
}
    

const mapStateToProps = store => ({
  address: store.addressState.address,
  restaurant: store.newOrderState.restaurant,
  order: store.newOrderState.order
});

const mapDispatchToProps = dispatch => bindActionCreators({ removeAllOrderItem}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(OrderForm);