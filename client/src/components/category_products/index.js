import React, {Fragment} from 'react';
import { Title, Column, Box, Image } from 'rbx';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { showModal } from "../../actions/modal";

import "../../styles/product.scss";

let oldPrice = [33,33,36,39,33,36];

const CategoryProducts = (props) => (
  
  <div id="product">
    <br></br>

    {props.show &&
    <Fragment>
    <Title size="5" className="category_title" id={props.title}>{props.title}</Title>
  
    <Column.Group gapSize={2} multiline >
      {props.products.map((product, i) => {        
        return (
          <Fragment key={i}>            
          {(product.availability > 0 && //Produto disponivel
          (props.title !== "Promoções"||//Tudo que não é promoção
          (props.title === "Promoções" && product.id === new Date().getDay() + 2)||//Promoção do dia
          (new Date().getDay() > 0 && new Date().getDay() < 5 && product.id === 1) //Combo
          //getDay = dia - 1 . eg. dom = 0, seg = 1, ter = 2
          ))&&
          
              <Column key={i} size="4">
                <Box>
                {/*eslint-disable-next-line*/}
                <a href="#" onClick={() => props.showModal('ADD_PRODUCT', {'product': product, 'restaurant': props.restaurant}) } >
                  <Column.Group>
                    <Column size="6" offset={1}>
                      <Title size="6">{product.name}</Title>
                      <Title subtitle size="6">{product.description}</Title>
                      {props.title === "Promoções" && product.id > 1 ?
                      <div className="promo_prices"><span className="dashed_box_red"><del>R${oldPrice[new Date().getDay() + 1]}</del></span> 
                      <span className="dashed_box">R${product.price}</span>
                      </div>:
                      <span className="dashed_box">R${product.price}</span>
                    }
                    </Column>
                    <Column size="two-fifths">
                      <Image src={product.image_url} width="50%" />
                    </Column>
                  </Column.Group>
                </a>
              </Box>
            </Column>
          }</Fragment>
        )
      })}
    </Column.Group>
    </Fragment>
    }
  </div>
);

const mapDispatchToProps = dispatch => bindActionCreators({ showModal }, dispatch);

export default connect(null, mapDispatchToProps)(CategoryProducts);