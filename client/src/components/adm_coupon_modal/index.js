import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Column, Title, Button, Field, Label, Control} from "rbx";
import { bindActionCreators } from 'redux';
import { setAddress } from "../../actions/address";
import { hideModal } from "../../actions/modal";
import api from '../../services/api';



class AdmProductModal extends Component {

  constructor(props) {
    super(props);

    this.state = {
      // params.require(:coupon).permit(:name, :type, :value, :min_value, :valid, :max_uses, :expiration_date)
   
      name: "",
      kind: 1,
      value: "",
      min_value: 0,
      works: 1,
      max_uses: 0,
      expiration_date: "",
    }


    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
  }
  

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleSubmit(event) {
    event.preventDefault();

    api.createNewCoupon(this.state);
    this.forceUpdate();
    this.props.hideModal('ADM_COUPON_MODAL')
    //createProduct()
  }

  

  render() {
    return (
      <Fragment>
        <Column.Group>
          <Column size={10} offset={1}>
            <Title autoFocus={true} id="modal_title" size={3} className="has-text-custom-gray-darker status_msg">
              Adicionar Cupom
            </Title>
            
            
                
          </Column>
          </Column.Group>

          <form onSubmit={this.handleSubmit}>
          <Field kind="group" align="centered">
        <Column.Group>
        <Column>
        
                <Control>

                  
                      
                <Label>Nome:</Label>
                      <input name="name" value={this.state.name} onChange={this.handleInputChange}>
                      </input>


                 <Label>Tipo:</Label>
                      <select name="type" value={this.state.kind} onChange={this.handleInputChange}>
                      <option value={1}>Percentual</option>
                      <option value={2}>Valor fixo</option>

                      </select>
                      
                      
                      <Label>Valor:</Label>
                      <input name="value" type="number" value={this.state.value} onChange={this.handleInputChange}>
                      </input> {this.state.kind===1 ? "%" : "R$" }

                      <Label>Compra mínima:</Label>
                      <input name="min_value" type="number" value={this.state.min_value} onChange={this.handleInputChange}>
                      </input>  
                      
                      
                      <Label>Limite de uso:</Label>
                      <input name="max_uses" value={this.state.max_uses} onChange={this.handleInputChange}>
                      </input> cupons

                      
                      <Label>Expiração:</Label>
                      <input name="expiration_date" type="date" value={this.state.expiration_date} onChange={this.handleInputChange}>
                      </input>


<br/>               
<br/> 
            </Control>         
                  <Button  onClick={ () => this.props.hideModal('ADM_COUPON_MODAL') }  size="medium" color="custom-orange">
                    <span className="has-text-white">Fechar</span> 
                    </Button>  
                    </Column>
                    <Column>                 
                  <Button size="medium" color="default-green">
                    <span className="has-text-white">Salvar</span>
                  </Button>
                  </Column>

                  </Column.Group>
              </Field>
              </form>
        
      </Fragment>
    )
  }

}

const mapStateToProps = store => ({
  address: store.addressState.address,
  modal: store.modalState,
  restaurant: store.newOrderState.restaurant,
});

const mapDispatchToProps = dispatch => bindActionCreators({ setAddress, hideModal }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AdmProductModal);