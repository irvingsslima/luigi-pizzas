import React, { Component } from 'react';
import "../../styles/categories.scss";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Card from 'react-bootstrap/Card';
import Table from 'react-bootstrap/Table';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faCheckCircle, faTrashAlt } from '@fortawesome/free-regular-svg-icons';

library.add(faCheckCircle, faTrashAlt)

class ListCoupons extends Component {
  state = { product: {} }

    constructor(props) {
        super(props);
        this.state = {
          coupons: []
        };
        //this.loadCoupons = this.loadCoupons.bind(this);
      }

  async checkCoupon(coupon) 
  {
      await fetch(`/api/coupons/${coupon.id}`,
        {
          method: 'PUT',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*' 
          },
          
          body: JSON.stringify(
              {'works': false}
          )        

        }
        
      )
      this.props.loadCoupons();
      this.forceUpdate()
    }

   render() {
     return (
       <div>
         <Card>
           <Card.Body>
           <Table responsive>
             <tbody>
               <tr>
                    <td className="col-md-10">ID</td> 
                   <td className="col-md-10">Código</td>
                   <td className="col-md-10">Tipo</td>
                   <td className="col-md-10">Desconto</td> 
                   
                                
                   <td className="col-md-10">Compra mínima</td>         
                   <td className="col-md-10">Expiração</td>
                   <td className="col-md-10">Cancelar</td>    
                   </tr>
               {this.props.coupons.map((coupon) => {
                 return <tr key={coupon.id}>
                   
                   <td className="col-md-10">{coupon.id}</td> 
                   <td className="col-md-10">{coupon.name}</td>
                   <td className="col-md-10">{coupon.kind}</td>
                   <td className="col-md-10">{coupon.value}{coupon.kind === 1 ? "%" : "R$"}</td> 
                   
                                
                   <td className="col-md-10">{coupon.min_value}</td>         
                   <td className="col-md-10">{coupon.expiration_date.toLocaleString("pt-BR")}</td>
                   <td>
                     { 
                  coupon.works
                   // eslint-disable-next-line
                      ? <a className="check" href="#">
                      <FontAwesomeIcon icon={['far', 'check-circle']} color="green" onClick={() => this.checkCoupon(coupon)} size="lg"/>
                      </a> 
                      : null
                     }
                   </td>
                   
                   
                 </tr>;
               })}
             </tbody>
           </Table>
           </Card.Body>
         </Card>
       </div>
     );
   }
}

const mapStateToProps = store => ({
  address: store.addressState.address
});

const mapDispatchToProps = dispatch => bindActionCreators({  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ListCoupons);