import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
//import SearchBox from '../search_box_component';
import { showModal } from "../../actions/modal";
import "../../styles/header.scss";

const MenuLine = (props) => (
  <div className="top-level">
      <div fixed="bottom" align="center" className="menu_line">        
      <div fixed="bottom" align="center" className="roll-link">        
      <a className="bottom-header-text" href="#Pizzas">Pizzas</a>  
      &nbsp;&nbsp;    &nbsp;       
      <a className="bottom-header-text" href="#Pizzas Doces">Pizzas Doces</a>  
      &nbsp;&nbsp;    &nbsp;
      <a className="bottom-header-text" href="#Bebidas">Bebidas</a>
    </div>
</div>
  </div>
);

const mapStateToProps = store => ({
  address: store.addressState.address,  
  order: store.newOrderState.order
});

const mapDispatchToProps = dispatch => bindActionCreators({ showModal }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(MenuLine);



