import React, { Component } from 'react';
import { connect } from 'react-redux';
import addressModal from './address';
import loginModal from './login';
import addProductModal from './add_product';
import go2cartModal from './go_to_cart';
import inOrderModal from './incoming_order';
import admProductModal from './adm_product';
import admCouponModal from './adm_coupon';
import aboutModal from './about';
//import {Modal} from 'rbx';

class ModalRoot extends Component {

  render() {
    const modal_components = {
      'ADDRESS_MODAL': addressModal,
      'ADD_PRODUCT': addProductModal,
      'LOGIN_MODAL': loginModal,
      'GO2CART_MODAL': go2cartModal,      
      'IN_ORDER_MODAL': inOrderModal,
      'ADM_PRODUCT_MODAL': admProductModal,
      'ABOUT_MODAL': aboutModal,
      'ADM_COUPON_MODAL': admCouponModal,

    }

    if (!this.props.modal.modalType) {
      return null
    } else {
      const Modal = modal_components[this.props.modal.modalType]
      return (
        <div>
        <div className="modal_root">
          
          <div className="modal-guts">
          <Modal {...this.props.modal.modalProps} />
          
          </div>
          <div className="modal-footer"></div>
        </div>        
    <div className="modal-overlay" id="modal-overlay"></div>
    </div>
      )
    }
  }
}

const mapStateToProps = store => ({
  modal: store.modalState
});

export default connect(mapStateToProps, null)(ModalRoot);