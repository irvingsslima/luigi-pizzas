import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Title, Column, Icon} from "rbx";
import { bindActionCreators } from 'redux';
import { hideModal } from "../../actions/modal";
import { FaTimes } from 'react-icons/fa';

class About extends Component {

  render() {
    return (
      <Column.Group centered>
      <Column size={12}  >
        <Title autoFocus={true} id="modal_title" size={3} className="has-text-custom-gray-darker status_msg">
         Sobre
        </Title>
            <Column.Group>
              <Column size={12} textAlign="right">
                <Icon color="has-custom-black" onClick={() => this.props.hideModal('IN_ORDER_MODAL')}>
                  <FaTimes />
                </Icon>
              </Column>
            </Column.Group>
        </Column>
      </Column.Group>
    )
  }

}

const mapDispatchToProps = dispatch => bindActionCreators({ hideModal }, dispatch);

export default connect(null, mapDispatchToProps)(About);