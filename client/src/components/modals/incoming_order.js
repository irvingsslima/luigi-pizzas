import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Box, Column, Icon} from "rbx";
import { bindActionCreators } from 'redux';
import { hideModal } from "../../actions/modal";
import { FaTimes } from 'react-icons/fa';
import InOrder from '../incoming_order'

class IncomingOrder extends Component {

  render() {
    return (
      <Column.Group centered>
        <Column size="6" mobile={{ 'size': 10, 'offset': 1 }}>
          <Box>
            <Column.Group>
              <Column size={12} textAlign="right">
                <Icon color="has-custom-black" onClick={() => this.props.hideModal('IN_ORDER_MODAL')}>
                  <FaTimes />
                </Icon>
              </Column>
            </Column.Group>
            <InOrder />
          </Box>
        </Column>
      </Column.Group>
    )
  }

}

const mapDispatchToProps = dispatch => bindActionCreators({ hideModal }, dispatch);

export default connect(null, mapDispatchToProps)(IncomingOrder);