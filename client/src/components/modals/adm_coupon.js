import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Column, Icon} from "rbx";
import { bindActionCreators } from 'redux';
import { hideModal } from "../../actions/modal";
import { FaTimes } from 'react-icons/fa';
import AdmCouponModal from '../adm_coupon_modal'

class Adm_Coupon_Modal extends Component {

  render() {
    return (
      <Column.Group centered>
        <Column size="6" mobile={{ 'size': 10, 'offset': 1 }}>
            <Column.Group>
              <Column size={12} textAlign="right">
                <Icon color="has-custom-black" onClick={() => this.props.hideModal('ADM_COUPON_MODAL')}>
                  <FaTimes />
                </Icon>
              </Column>
            </Column.Group>
            <AdmCouponModal />
        </Column>
      </Column.Group>
    )
  }

}

const mapDispatchToProps = dispatch => bindActionCreators({ hideModal }, dispatch);

export default connect(null, mapDispatchToProps)(Adm_Coupon_Modal);