import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Column, Icon} from "rbx";
import { bindActionCreators } from 'redux';
import { hideModal } from "../../actions/modal";
import { FaTimes } from 'react-icons/fa';
import AddProductForm from '../add_product_form'

class AddProductModal extends Component {
  render() {
    return (
      <Column.Group centered>
        <Column size="10" mobile={{ 'size': 10, 'offset': 1 }}>          
            <Column.Group>
              <Column size={12} textAlign="right">
                <Icon color="has-custom-black" className="close-button" onClick={() => this.props.hideModal('ADDRESS_MODAL')}>
                  <FaTimes />
                </Icon>
              </Column>
            </Column.Group>
            <AddProductForm restaurant={this.props.restaurant} product={this.props.product} />
          
        </Column>
      </Column.Group>
    )
  }

}

const mapDispatchToProps = dispatch => bindActionCreators({ hideModal }, dispatch);

export default connect(null, mapDispatchToProps)(AddProductModal);