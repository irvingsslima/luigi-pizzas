import React, { Component, Fragment } from 'react';
import { Title, Column, Button, Field, Label, Control, Input } from "rbx";
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { removeOrderItem } from "../../actions/new_order";
import { removeHalfOrderItem } from "../../actions/new_order";
import { removeAllOrderItem } from "../../actions/new_order";
import { hideModal } from "../../actions/modal";
import "../../styles/order.scss";
import api from "../../services/api";
import history from '../../history';
import businessHours from "business-hours.js";
import hoursJson from "../../hours.json";
import { addOrderItem } from "../../actions/new_order";
import axios from "axios";
import distFrom from 'distance-from';


const google_key = 'AIzaSyBXQBVM5AF3ZYwXYKtxdMFuh6Q1eRsrZJM'; 

var validAddress = false;
let paymentScreen = false;
let halfPizza = false;
let coupon = false;
let distance_km = "";



const limiteEntrega = 7;



class Order extends Component {



  constructor(props) {
    super(props);

    this.state = {
      name:  '',
      cep:   '',
      restaurant_id: 1,
      delivery_tax: 99,
      coupon: '',
      couponValue: '',
      couponName: '',
      couponValid: false,
      couponError: ''
    }

    
  this.handleSubmit = this.handleSubmit.bind(this);
  this.handleInputChange = this.handleInputChange.bind(this);
  }

  UNSAFE_componentWillMount() {
    businessHours.init(hoursJson);
    paymentScreen = false;    
    validAddress = false;
  }

/*
  async consultaCEP(args) {
    let arg = Object.assign({}, args)

    if ('cep' in arg === false) {
      throw new Error('You need to inform a CEP ex: { cep: 00000000 }');
    }

    //return new Promise((resolve, reject) => {
      const cepUrl = 'https://viacep.com.br/ws/{CEP}/json';
      let url = cepUrl.replace('{CEP}', arg.cep.replace('-', ''));

      return axios.get(url)

  }

*/





deliveryCalc = (cep_destino) => { 
  let destino = "";


  const cepUrl = 'https://viacep.com.br/ws/{CEP}/json';
    let url = cepUrl.replace('{CEP}', cep_destino.replace('-', ''));

  axios.get(url).then(result => {
    console.log("RESPONSE CEP: " + JSON.stringify(result))

    destino = `${result.data.logradouro}, ${result.data.bairro}, ${result.data.localidade}, ${result.data.uf}`
    
   const geoUrl = 'https://maps.googleapis.com/maps/api/geocode/json?address={destino}&key={key}';

  
    let url2 = geoUrl.replace('{destino}', destino);
    url2 = url2.replace('{key}', google_key);

    axios.get(url2).then(response => {

      const luigi = [-22.547550, -44.162922];

      console.log("LAT E LONG: "+ response.data.results[0].geometry.location.lat + " , "+ response.data.results[0].geometry.location.lng)

      const cliente = [response.data.results[0].geometry.location.lat, response.data.results[0].geometry.location.lng];
      
      
      //distance_km = distFrom(luigi).to(cliente).in('km');

      //console.log(distance_km);


    return distFrom(luigi).to(cliente).in('km');;
      
    
    }  )

  });

  }







  sendData = () => {
    //(childData, deliveryTax, cep_value, total)
    if(this.state.couponValue === 57){
      this.props.parentCallback(0, this.state.cep, this.totalCart().toFixed(2));
  }
  else
    this.props.parentCallback(this.state.delivery_tax, this.state.cep, this.totalCart().toFixed(2));
};

  handleInputChange(event) {


    const target = event.target;
    const value = target.value;
    const name = target.name;


    this.setState({
      [name]: value
    });

    //if(name === "cep" && value.length > 7){
    //}

  }

  handleBackButton(event) {
    
    paymentScreen = false;    
    validAddress = false;


    this.setState({        
      delivery_tax: 99   
    }) 
  }

  

  handleSubmit(event) {
    console.log("Submitted; ")
    event.preventDefault();

    if(paymentScreen){  
      api.createOrder(this.state, this.props.order, this.props.address).then((response) => {
        let id = response.data.order.id 
        if(response.status === 201){        
          
        console.log("Response; "+response)    
        history.push(`/orders/${id}`)      
        this.removeAllItems()
        paymentScreen = false;
        }
        else
        console.log("Erro! "+response.status, response.error)
      }
      
      
      )



    }
    else{//Não está na tela de pagamento

      if(!validAddress)//Ainda não possui endereço v;alido
      {  
          
        let destino = "";
        
        
          const cepUrl = 'https://viacep.com.br/ws/{CEP}/json';
            let url = cepUrl.replace('{CEP}', this.state.cep.replace('-', ''));
        
          axios.get(url).then(result => {
            console.log("RESPONSE CEP: " + JSON.stringify(result))
        
            destino = `${result.data.logradouro}, ${result.data.bairro}, ${result.data.localidade}, ${result.data.uf}`
            
           const geoUrl = 'https://maps.googleapis.com/maps/api/geocode/json?address={destino}&key={key}';
        
          
            let url2 = geoUrl.replace('{destino}', destino);
            url2 = url2.replace('{key}', google_key);
        
            axios.get(url2).then(response => {
        
              const luigi = [-22.547550, -44.162922];
        
              console.log("LAT E LONG: "+ response.data.results[0].geometry.location.lat + " , "+ response.data.results[0].geometry.location.lng)
        
              const cliente = [response.data.results[0].geometry.location.lat, response.data.results[0].geometry.location.lng];
              
              
              distance_km = distFrom(luigi).to(cliente).in('km');
        
              //console.log(distance_km);
        
        
            //return distFrom(luigi).to(cliente).in('km');;


            if(distance_km > 0){
            
              if(2*Math.round(distance_km) <= limiteEntrega){
  
            validAddress = true;
  
            console.log("******ResultData:   " + distance_km)
            this.setState({        
              delivery_tax: (2*Math.round(distance_km))    
            })
          }
          else
          {
            this.setState({        
              delivery_tax: -7          
             
            })
            validAddress = false;
  
          }
  
            console.log("Delivery_tax; "+this.state.delivery_tax);
  
          }
          else{ //Resultado da pesquisa no Google nao retornou OK
            this.setState({        
              delivery_tax: -3            
             
            })
            validAddress = false;
          }
              
            
            }  )
        
          });
        











        
          
        

        // console.log("******ResultData:   " + result)










          
      }
      //Else do if !validAddress
      else
      {

        if(validAddress)
      console.log("Changing to payment screen order")
      paymentScreen = !paymentScreen
      this.forceUpdate()  


      }    
    
  }

  }


  orderItemAmount = (orderItem) => {
    return orderItem.quantity * orderItem.product.price
  } 

  totalOrder = () => {   

    const arrayToSum = this.props.order.map((orderItem) => { return this.orderItemAmount(orderItem) })
    if(this.state.couponValue === 33)
    return 0.9*arrayToSum.reduce((a, b) => a + b, 0)
    else if(this.state.couponValue === 57 && this.state.delivery_tax !== 99)
    return arrayToSum.reduce((a, b) => a + b, 0) - this.state.delivery_tax
    else
    return arrayToSum.reduce((a, b) => a + b, 0)
  }

  totalCart = () => {
    return this.totalOrder() + this.state.delivery_tax
  }

  removeItem = (orderItem) => {
    this.props.removeOrderItem(orderItem);
  }

  removeHalfItem = (orderItem) => {
    this.props.removeHalfOrderItem(orderItem);
  }

  removeAllItems = () => {
    this.props.removeAllOrderItem();
  }

  closeOrder = () => {
    this.props.hideModal('ORDER_MODAL');
  }


  removeCoupon() {
    this.setState({        
      coupon: false,
      couponValue: -3,
      couponName: '',
      couponValid: false,
      couponError: ''
    }) 
  }

handleCoupon = () => {
  if(this.state.coupon.toUpperCase() === "PRIMEIRAVEZ")
  {    
    if(!localStorage.getItem('PRIMEIRAVEZ')){
      localStorage.setItem('PRIMEIRAVEZ', 171)
      this.setState({
        couponValid: true,
        couponValue: -10,
        coupon: ''
    
    
    })
    
  this.props.addOrderItem(this.props.restaurant, this.props.restaurant.product_categories[6].products[0], 1, this.state.comment)
  coupon = true;
  }else{
    this.setState({
      couponValid: false,
      couponError: "Cupom já utilizado!",
      coupon: ''
  
  
  })

  }
  

  }else{
    this.setState({
      couponValid: false,
      couponError: "Cupom inválido!",
      coupon: ''
  })

  }


  if(this.state.coupon.toUpperCase() === "MAE10")
  {    
    if(!localStorage.getItem('MAE11')){
      localStorage.setItem('MAE10', 171)
      this.setState({
        couponValid: true,
        couponValue: 33,
        couponName: "MAE10",
        coupon: ''
    
    
    })
    
  coupon = true;
  }else{
    this.setState({
      couponValid: false,
      couponError: "Cupom já utilizado!",
      coupon: ''
  
  
  })

  }
  

  }else{
    this.setState({
      couponValid: false,
      couponError: "Cupom inválido!",
      coupon: ''
  })

  }





  if(this.state.coupon.toUpperCase() === "FRETEGRATIS")
  {    
    if(!localStorage.getItem('FRETEGRATIS')){
      localStorage.setItem('FRETEGRAxxxTIS', 171)
      this.setState({
        couponValid: true,
        couponValue: 57,
        couponName: "FRETEGRATIS",
        coupon: ''
    
    
    })
    
  coupon = true;
  }else{
    this.setState({
      couponValid: false,
      couponError: "Cupom já utilizado!",
      coupon: ''
  
  
  })

  }
  

  }else{
    this.setState({
      couponValid: false,
      couponError: "Cupom inválido!",
      coupon: ''
  })

  }





  console.log(this.props.restaurant, this.props.restaurant.product_categories[6].products[0])
  this.forceUpdate();
}

  render() {
    
    if(this.props.restaurant === undefined || this.props.order.length <= 0){
      return (
        <Column size={10}>
          <Title size={5} className="has-text-custom-gray-darker">Carrinho Vazio</Title>
          <Title size={6} subtitle className="has-text-custom-gray-darker">Escolha um produto</Title>
          <br/>
        </Column>
      )
    } else{
      return (
        <Fragment>
          <Column size={12}>
          
            <div className="order-box">
              <Title size={5} className="has-text-custom-green-darker">{ this.props.restaurant.name }</Title>
              <hr />    
              { this.props.order.map((orderItem, i) => {

                console.log("Debug erro if promocao");

                console.log(orderItem.product.product_category.title);

                console.log("Order item completo");
                console.log(orderItem);


                //if(orderItem.product.product_category.title === "Promoção do dia" && orderItem.product.id !== (new Date().getDay() + 1))
                  //  this.removeItem(orderItem)

                  if(orderItem.quantity === 0.5)
                      halfPizza = !halfPizza 

                return  <Fragment key={i}>
                          <Column.Group multiline gapless>
                            <Column size="four-fifths">
                            <Title size={6} className="has-text-default-green">
                                {orderItem.quantity > 0.5 ? orderItem.quantity+"x" : "1/2"} {orderItem.product.name}
                              </Title>
                            </Column>
                            <Column size="one-fifth" className="has-text-weight-bold has-text-right">
                              R${ this.orderItemAmount(orderItem).toFixed(2) }
                            </Column>
                            <Column size="three-fifths">
                              <Title size={7} className="has-text-weight-normal has-text-custom-gray-darker">
                              
                                {orderItem.product.name === "LUIGI COMBO" ? orderItem.comment :
                                orderItem.product.description
                                 }

                              </Title>
                            </Column>
                            <Column size="two-fifth" className="has-text-right">
                              {orderItem.quantity === 0.5 ?
                              <div>{!halfPizza && <button className="dashed_box is-size-7 remove-button" 
                                      
                                      onClick={ () => this.removeHalfItem(orderItem) }>
                                Remover
                              </button>
                            }</div>:
                              
                              <button className="dashed_box is-size-7 remove-button" 
                                      
                                      onClick={ () => this.removeItem(orderItem) }>
                                Remover
                              </button>


                              }


                          
                            </Column>
                          </Column.Group>











                        </Fragment>
              }) }

{this.state.couponValue === 57 && 
                          <Column.Group multiline gapless>
                            <Column size="four-fifths">
                            <Title size={6} className="coupon-red" color="red">
                                CUPOM FRETEGRATIS
                                {//"1"} {orderItem.product.name
                                }
                              </Title>
                            </Column>


                            {this.state.delivery_tax !== 99 &&
                            <Column size="one-fifth" className="has-text-weight-bold has-text-right">

R${ (-1*this.state.delivery_tax).toFixed(2) }
                            </Column>
    }
                            <Column size="three-fifths">
                                
                                {
                                this.state.delivery_tax !== 99 ?
                                <Title size={7} className="has-text-weight-normal has-text-custom-gray-darker">
                              
                                  Frete grátis! 
                                  </Title>
                                  :
                                  <Title size={7} className="has-text-weight-normal has-text-custom-gray-darker">
                              
                              Digite primeiro o CEP!
                              </Title>
                                }
                                

                              
                            </Column>
                            <Column size="two-fifth" className="has-text-right">
                              <button className="dashed_box is-size-7 remove-button" 
                                      
                                      onClick={ () => this.removeCoupon() }>
                                Remover
                              </button>




                          
                            </Column>
                          </Column.Group>
                        }

{this.state.couponValue === 33 && 
                          <Column.Group multiline gapless>
                            <Column size="four-fifths">
                            <Title size={6} className="coupon-red" color="red">
                                CUPOM MAE10
                                {//"1"} {orderItem.product.name
                                }
                              </Title>
                            </Column>
                            <Column size="one-fifth" className="has-text-weight-bold has-text-right">

R${ (-1*this.totalOrder()/9).toFixed(2) }
                            </Column>
                            <Column size="three-fifths">
                              <Title size={7} className="has-text-weight-normal has-text-custom-gray-darker">
                                10% de desconto no Dia das Mães!

                              </Title>
                            </Column>
                            <Column size="two-fifth" className="has-text-right">
                              <button className="dashed_box is-size-7 remove-button" 
                                      
                                      onClick={ () => this.removeCoupon() }>
                                Remover
                              </button>




                          
                            </Column>
                          </Column.Group>
                        }
    
              <hr />

              <Column size="two-fifth" className="has-text-right">
                              <button className="dashed_box is-size-7 remove-button" 
                                      onClick={ () => this.removeAllItems() }>
                                Limpar carrinho
                              </button>
                            </Column>
    
              <Column.Group className="subtotal">
                <Column size="three-fifths">
                  <Title size={6} className="has-text-custom-gray-darker">
                    Subtotal
                  </Title>
                </Column>

                <Column size="two-fifths">
                  <Title size={6} className="has-text-custom-gray-darker has-text-right">
         

             <span>R${ this.totalOrder().toFixed(2) }</span>

                  </Title>
                </Column>

              </Column.Group>
              <hr/>
              
            
{!coupon &&
<Fragment>
              <Field>
                    <Label>Adicionar cupom?</Label>
                    <Control>
                      <Input
                        icon="FaCheck"
                        type="text"
                        //placeholder=''
                        maxLength="12"
                        size="12"
                        name="coupon"
                        value={this.state.coupon}
                        onChange={this.handleInputChange}       
                        /> 
                        </Control>
                        </Field> 
                        <Button size="medium" color="default-green" onClick={ () => this.handleCoupon()}>
                                <span className="has-text-white" >Adicionar Cupom</span>
                              </Button>
                    
                    {console.log(this.props)}
                    {this.state.couponError && <p className="dashed_box_red">{this.state.couponError}</p>}
                                 
                  
                             </Fragment>

}







{this.state.delivery_tax === 99 ? 
<Fragment>
<form onSubmit={this.handleSubmit}>
              <Field>
                    <Label>CEP</Label>
                    <Control>
                      <Input id='CEP_input'
                        type="number"
                        placeholder='27345000'
                        maxLength="9"
                        size="10"
                        name="cep"
                        value={this.state.cep}
                        onChange={this.handleInputChange}       
                        />
                    </Control>
                  </Field>
                  
                  <Button size="medium" color="default-green">
                                <span className="has-text-white" >Calcular Entrega</span>
                              </Button>
                              </form>
                              </Fragment>


                              :
                              <Fragment>
                                {this.state.delivery_tax < 0 ? 
                                  <Fragment>
                                    <form onSubmit={this.handleSubmit}>
              <Field>
                    <Label>CEP</Label>
                    <Control>
                      <Input id='CEP_input'
                        type="number"
                        placeholder='27345000'
                        maxLength="9"
                        size="10"
                        name="cep"
                        value={this.state.cep}
                        onChange={this.handleInputChange}       
                        />
                    </Control>
                  </Field>
                  
                  <Button size="medium" color="default-green">
                                <span className="has-text-white" >Calcular Entrega</span>
                              </Button>
                              </form>
                                    <Title size={6} className="has-text-custom-gray-darker has-text-right">
                                      <hr/>
                    <p>Infelizmente não entregamos nessa localidade, ou o CEP informado ({this.state.cep}) está incorreto.</p>
                  </Title>
                                  
                                  </Fragment>
                                  :
                              
                              
                              <Fragment>                                
          <form>
            <Column.Group>
                <Column size="three-fifths">
                  <Title size={6} className="has-text-custom-gray-darker">
                    Taxa de Entrega
                  </Title>
                </Column>
                <Column size="two-fifths">
                  <Title size={6} className="has-text-custom-gray-darker has-text-right">                    

                  { this.state.delivery_tax !== 0 ? <div>{"R$ "+ this.state.delivery_tax}</div>: <div className="dashed_box">Frete Grátis!</div>
                  }
                    
                  </Title>
                </Column>
              </Column.Group>
    
              <hr />
    
              <Column.Group>
                <Column size="three-fifths">
                  <Title size={6} className="has-text-custom-gray-darker">
                    Total
                  </Title>
                </Column>
                <Column size="two-fifths">
                  <Title size={6} className="has-text-custom-gray-darker has-text-right">

                  {
                  
                  <span>R${ this.totalCart().toFixed(2) }</span>
                  /*
                  this.state.couponValue === 33 ?

<span>R${ this.totalCartDiscount().toFixed(2) }</span>
             :

             <span>R${ this.totalCart().toFixed(2) }</span>

*/
            }

                    

                  </Title>
                </Column>
              </Column.Group>
              <Column size={12} align="center">
    <Button size="medium" onClick={ () => this.handleBackButton()} color="custom-orange" className="has-text-white" >
      Voltar
    </Button>
  </Column>

{businessHours.isOpenNow() &&  this.state.delivery_tax >= 0? 
  <Column size={12} align="center">
    <Button type='button' size="medium" color="default-green" className="has-text-white" onClick={ () => this.sendData()}  >
      Continuar
    </Button>
  </Column> :
  <div>
    <Title size={6} className="has-text-custom-gray-darker has-text-right">

    Infelizmente o restaurante está fechado no momento, obrigado!

</Title>

  </div>


}
  </form>

                              </Fragment>
                              }
                              </Fragment> }





              





            </div>
          </Column>
          
        </Fragment>
      )
    }
  }
}

const mapStateToProps = store => ({
  restaurant: store.newOrderState.restaurant,
  order: store.newOrderState.order
});

const mapDispatchToProps = dispatch => bindActionCreators({ removeAllOrderItem, removeOrderItem, removeHalfOrderItem, hideModal, addOrderItem }, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(Order);