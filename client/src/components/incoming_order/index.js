import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Column, Title, Button, Field} from "rbx";
import { bindActionCreators } from 'redux';
import { setAddress } from "../../actions/address";
import { hideModal } from "../../actions/modal";
import history from '../../history';
import Alert from '../../assets/sounds/alert.wav';
import Sound from 'react-sound';



class IncomingOrder extends Component {

  constructor(props) {
    super(props);

    this.state = {
    }


  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleSubmit(event) {
    this.props.hideModal('IN_ORDER_MODAL')
    event.preventDefault();
  }


  handleGo2Cart(event){      
      history.push('/orders') 
      this.forceUpdate()
      this.props.hideModal('IN_ORDER_MODAL')
  };

  render() {
    return (
      <Fragment>
        <Column.Group>
          <Column size={10} offset={1}>
            <Title autoFocus={true} id="modal_title" size={3} className="has-text-custom-gray-darker status_msg">
              Pedido!
            </Title>
            {this.props.modal.modalProps}
            
            {/*
            newText = this.props.modal.modalProps.split('\n').map((item, i) => {
    return <p key={i}>{item}</p>;
        });
            }
            </Fragment>
                        
          */}

{/*div_pedido = (this.props.modal.modalProps).split(';')   }


      <div>
      <br>
      {this.props.modal.modalProps}
      </br>  
         
      

        
        
  </div>
    */}

      <Sound
        url={Alert}
        playStatus={Sound.status.PLAYING}
        />
     
          </Column>
          </Column.Group>

          <Field kind="group" align="centered">
        <Column.Group>
        <Column>
                  <Button  onClick={ () => this.props.hideModal('IN_ORDER_MODAL') }  size="medium" color="custom-orange">
                    <span className="has-text-white">Fechar</span> 
                    </Button>  
                    </Column>
                    <Column>                 
                  <Button onClick={ () => this.handleGo2Cart() } size="medium" color="default-green">
                    <span className="has-text-white">Abrir orders!</span>
                  </Button>
                  </Column>

                  </Column.Group>
              </Field>
        
      </Fragment>
    )
  }

}

const mapStateToProps = store => ({
  address: store.addressState.address,
  modal: store.modalState
});

const mapDispatchToProps = dispatch => bindActionCreators({ setAddress, hideModal }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(IncomingOrder);