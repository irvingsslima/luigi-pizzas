import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Column, Title, Button, Field} from "rbx";
import { bindActionCreators } from 'redux';
import { setAddress } from "../../actions/address";
import { hideModal } from "../../actions/modal";
import history from '../../history';


class GoToCart extends Component {

  constructor(props) {
    super(props);

    this.state = {
    }


  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleSubmit(event) {
    this.props.hideModal('GO2CART_MODAL')
    event.preventDefault();
  }


  handleGo2Cart(event){      
      history.push('/orders/new') 
      this.props.hideModal('GO2CART_MODAL')
  }

  render() {
    return (
      <Fragment>
        
        <Column.Group>
          <Column size={12}  >
            <Title autoFocus={true} id="modal_title" size={3} className="has-text-custom-gray-darker status_msg">
              Deseja continuar comprando?
            </Title>
          </Column>
          </Column.Group>

          <Field kind="group" align="centered">
        <Column.Group>
        <Column>
                  <Button  onClick={ () => this.props.hideModal('GO2CART_MODAL') }  size="medium" color="custom-orange">
                    <span className="has-text-white">Continuar comprando</span> 
                    </Button>  
                    </Column>
                    <Column>                 
                  <Button onClick={ () => this.handleGo2Cart() } size="medium" color="default-green">
                    <span className="has-text-white">Ir para o carrinho!</span>
                  </Button>
                  </Column>

                  </Column.Group>
              </Field>
        
      </Fragment>
    )
  }

}

const mapStateToProps = store => ({
  address: store.addressState.address
});

const mapDispatchToProps = dispatch => bindActionCreators({ setAddress, hideModal }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(GoToCart);