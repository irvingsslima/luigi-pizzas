import React, { Component } from 'react';
import "../../styles/categories.scss";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { loadOrders } from '../../actions/open_orders';
import Card from 'react-bootstrap/Card';
import Table from 'react-bootstrap/Table';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faCheckCircle, faTrashAlt } from '@fortawesome/free-regular-svg-icons';

library.add(faCheckCircle, faTrashAlt)

class ListOrders extends Component {
  async checkOrder(order) 
  {
      await fetch(`/api/orders/${order.id}`,
        {
          method: 'PUT',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*' 
          },
          
          body: JSON.stringify(
              {'status': "delivered"}
          )        

        }
        
      )
      this.props.loadOrders();
      this.forceUpdate()
    }

   render() {
     return (
       <div>
         <Card>
           <Card.Body>
           <Table responsive>
             <tbody> 
               <tr>
                   <td className="col-md-10">ID</td> 

                   <td className="col-md-10">Horário</td> 
                   <td className="col-md-10">Cliente</td> 
                   <td className="col-md-10">Telefone</td> 
                   
                                
                   <td className="col-md-10">Endereço</td>         
                   <td className="col-md-10">Valor total</td> 
                   <td className="col-md-10">Pagamento</td>     
                   <td className="col-md-10" align="center">Pedido</td> 
                   <td className="col-md-10">Pronto?</td> 
                   </tr>
               {this.props.orders.map((order, i) => {
                 return <tr key={i}>
                   
                   <td className="col-md-10">{order.id}</td> 
                   <td className="col-md-10">{new Date(order.order_products[0].created_at).toLocaleString()}</td> 
                   <td className="col-md-10">{order.name}</td> 
                   <td className="col-md-10">{order.phone_number}</td> 
                   
                                
                   <td className="col-md-10">{order.address}</td>         
                   <td className="col-md-10">R$ {order.total_value}</td> 

                   {
                   //<td className="col-md-10">R$ {order.total_value_discount}</td> 
               }
                   <td className="col-md-10">{order.change === -13?
                   <span>Cartão</span>:
                  <span>
                  {order.change > order.total_value ? 

                   <span>Troco pra {order.change} (R$ {order.change - order.total_value})</span>:

                   <span>Não informado</span>}
                   </span>
                  
                  
                  
                  
                  }
                   </td> 
                  
                  <td> {order.order_products.map((order_products, i) =>{
    return <tr key={i}>
      {console.log("--- " + i + "----  " + JSON.stringify(order_products))}


                        {
                          order_products.product_category > 4 && <td className="col-md-10"> -> </td>
                        }
      <td className="col-md-10">
        {order_products.product_category === 6? <span>Borda de </span> : order_products.quantity}
      </td>
      <td className="col-md-10">{order_products.name}</td>         
  <td className="col-md-10">{order_products.comment}</td> 
  </tr> 
  })}
  </td>
                   
                   
                   
                       
                   <td>
                     { 
                  order.status === "waiting"
                   // eslint-disable-next-line
                      ? <a className="check" href="#">
                      <FontAwesomeIcon icon={['far', 'check-circle']} color="green" onClick={() => this.checkOrder(order)} size="lg"/>
                      </a> 
                      : null
                     }
                   </td>
                   
                   
                 </tr>;
               })}
               {
      console.log("props loadOrders: "+JSON.stringify(this.props))}
             </tbody>
           </Table>
           </Card.Body>
         </Card>
       </div>
     );
   }
}

const mapStateToProps = store => ({
  address: store.addressState.address
});

const mapDispatchToProps = dispatch => bindActionCreators({ loadOrders }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ListOrders);