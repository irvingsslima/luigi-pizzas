import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Column, Title, Button, Field, Label, Control} from "rbx";
import { bindActionCreators } from 'redux';
import { setAddress } from "../../actions/address";
import { hideModal } from "../../actions/modal";
import api from '../../services/api';



class AdmCouponModal extends Component {

  constructor(props) {
    super(props);

    this.state = {
      //:id, :name, :description, :price, :availability, :product_category, :image_url
      name: "",
      description: "",
      price: "",
      availability: 1,
      product_category: { 
        id: "1",
      },
      image: ""
    }


    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleChange = this.handleChange.bind(this)
  }
  

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleChange(event) {
    this.setState({
      image:  event.target.files[0]
    })
    
    console.log("EVENT: "+JSON.stringify(event.target.files[0]))
    console.log(this.state)
  }

  handleSubmit(event) {
    event.preventDefault();

    api.createNewProduct(this.state);
    this.props.hideModal('ADM_PRODUCT_MODAL')
    //createProduct()
  }

  

  render() {
    return (
      <Fragment>
        <Column.Group>
          <Column size={10} offset={1}>
            <Title autoFocus={true} id="modal_title" size={3} className="has-text-custom-gray-darker status_msg">
              Adicionar Produto
            </Title>
            
            
                
          </Column>
          </Column.Group>

          <form onSubmit={this.handleSubmit}>
          <Field kind="group" align="centered">
        <Column.Group>
        <Column>
        
                <Control>


                <Label>Categoria:</Label>
                      <select name="product_category.id" value={this.state.value} onChange={this.handleInputChange}>
                        {this.props.restaurant.product_categories.map((category) => {                          
                        return (
                          <option value={category.id}>{category.title}</option>
                        )
                        })};

                      </select>
                      
                      <Label>Nome:</Label>
                      <input name="name" value={this.state.value} onChange={this.handleInputChange}>
                      </input>
                      
                      
                      <Label>Preço:</Label>
                      <input name="price" value={this.state.value} onChange={this.handleInputChange}>
                      </input>
                      
                      
                      <Label>Descrição:</Label>
                      <input name="description" value={this.state.value} onChange={this.handleInputChange}>
                      </input>

                      
                      <Label>Imagem:</Label>
                      <div>
        <input type="file" onChange={this.handleChange}/>
      </div>

<div className="preview">
        <img src={this.state.image_url} alt=""/>
      </div>



                </Control>         
                  <Button  onClick={ () => this.props.hideModal('ADM_PRODUCT_MODAL') }  size="medium" color="custom-orange">
                    <span className="has-text-white">Fechar</span> 
                    </Button>  
                    </Column>
                    <Column>                 
                  <Button size="medium" color="default-green">
                    <span className="has-text-white">Salvar</span>
                  </Button>
                  </Column>

                  </Column.Group>
              </Field>
              </form>
        
      </Fragment>
    )
  }

}

const mapStateToProps = store => ({
  address: store.addressState.address,
  modal: store.modalState,
  restaurant: store.newOrderState.restaurant,
});

const mapDispatchToProps = dispatch => bindActionCreators({ setAddress, hideModal }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AdmCouponModal);