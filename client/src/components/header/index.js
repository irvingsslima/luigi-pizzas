import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Navbar, Container, Icon, Dropdown, Button} from 'rbx';
import LogoImage from '../../assets/images/luigi.jpg';
import { showModal } from "../../actions/modal";
import { FaAngleDown, FaShoppingBasket, FaCog } from 'react-icons/fa';
import history from '../../history';
import "../../styles/header.scss";
import NotificationBadge from 'react-notification-badge';
import {Effect} from 'react-notification-badge';


const Header = (props) => (
  <div className="top-navbar">
    <Container>
      <Navbar fixed="top">
          <Navbar.Brand >
          <img src={LogoImage} alt="Luigi Pizzas logo" onClick={e => history.push('/')}/>
            <Navbar.Segment as="div"  className="navbar-item navbar-icons" align="start">              
              <Navbar.Item onClick={ () => history.push('/orders/new') }  className="navButton" >
                <Icon color="white">
                  
                  <FaShoppingBasket>     
                            
                  </FaShoppingBasket>
                  
                </Icon>
                
              <NotificationBadge count={props.order.length} className={'abc'} effect={Effect.SCALE} frameLength={60.0}/>     
                  <p>Sacola</p>
                
              </Navbar.Item>

              {localStorage.getItem("@luigipizzas/jwt") && 
              <Dropdown>
    <Dropdown.Trigger>
      <Button background-color="green">
                <Icon color="black">
                  <FaCog />
                </Icon>
                <span>Admin</span>
        <Icon size="small">
          <FaAngleDown/>
        </Icon>
      </Button>
    </Dropdown.Trigger>
    <Dropdown.Menu>
      <Dropdown.Content>
        <Dropdown.Item onClick={ () => history.push('/orders') }>Pedidos</Dropdown.Item>
        <Dropdown.Item onClick={ () => history.push('/products') }>Produtos</Dropdown.Item>
        <Dropdown.Item onClick={ () => history.push('/coupons') } >Cupons</Dropdown.Item>
        <Dropdown.Item onClick={ () => history.push('/sales') }>PDV</Dropdown.Item>
        <Dropdown.Divider />
        <Dropdown.Item onClick={() => props.showModal('ABOUT_MODAL')}>Sobre...</Dropdown.Item>
      </Dropdown.Content>
    </Dropdown.Menu>
  </Dropdown>
  
              }

              
            </Navbar.Segment>

          </Navbar.Brand>
          <Navbar.Menu >
            {/*}
            <Navbar.Segment as="div" align="end" className="navbar-item navbar-end">
              <SearchBox />
            </Navbar.Segment>
            */}
            </Navbar.Menu>
            
            


      </Navbar>
    </Container>
  </div>
);

const mapStateToProps = store => ({
  address: store.addressState.address,  
  order: store.newOrderState.order
});

const mapDispatchToProps = dispatch => bindActionCreators({ showModal }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Header);



