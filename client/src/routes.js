import React from 'react'
import { Router, Switch, Route } from 'react-router-dom'
import ShowExactRestaurant from './screens/show_exact_restaurant';
import CreateOrder from './screens/create_order';
import ShowOrder from './screens/show_order';
import ShowAdmin from './screens/admin';
import OpenOrders from './screens/show_open_orders';
import AdmCoupons from './screens/adm_coupons';
import AdmProducts from './screens/adm_products';
import history from './history';

const Routes = () => (
  <Router  history={history}>
    <Switch>
      <Route exact path='/' component={ShowExactRestaurant}  />     
      <Route exact path='/orders/new' component={CreateOrder} />
      <Route exact path='/orders/:id' component={ShowOrder} />
      <Route exact path='/admin' component={ShowAdmin} />
      <Route exact path='/orders' component={OpenOrders} />
      <Route exact path='/products' component={AdmProducts} />
      <Route exact path='/coupons' component={AdmCoupons} />
    </Switch>
  </Router >
)

export default Routes;