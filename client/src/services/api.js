import axios from "axios";

//const url = axios.create({ baseURL:" https://luigipizzas.herokuapp.com" })

const url = axios.create();

export default {

  loadRestaurants: (address, category) => {
    let city_params = (address && address.city !== undefined) ? `city=${address.city}` : ''
    let category_params = (category != null) ? `&category=${category.title}` : ''

    return url.get(`/api/restaurants?${city_params}${category_params}`)
  },
  
  searchProducts: (search) => url.get(`/api/products/search?q=${search}`),  
  
  getRestaurant: () => url.get(`/api/restaurants/1`),

  createOrder: (order, products_order, address) => {    
    let full_address = [  
      address.street, address.number, address.extra, address.reference, address.city,
      address.state, address.cep,
      //(address.reference)? `Referência: ${address.reference}` :  null,
      //(address.complement)? `Complemento: ${address.complement}` : null
      //TODO: Acertar complemento e referencia e bairro
      //
      //'product_category': product_order.product_category.title,
    ].join(',')


    console.log("----ORDER:-----"+ JSON.stringify(order));

    let new_product_orders = products_order.map(function (product_order) {
      console.log("----PRODUCT ORDER:-----"+ JSON.stringify(product_order));
      return ({
        'product_id': product_order.product.id,
        'name': product_order.product.name,  
        'product_category': product_order.product.product_category.id,
        'comment': product_order.comment,
        'quantity': product_order.quantity,
        'delivery_tax': product_order.delivery_tax,
        'change': order.change
      })
    });  
        
    order['order_products_attributes'] = new_product_orders
    order['address'] = full_address


    return url.post(`/api/orders`, order)
  },

  createNewProduct: (productData) => {  
  console.log(productData)
  return url.post(`/api/products`, productData)
  },

  createNewCoupon: (couponData) => { 
  return url.post(`/api/coupons`, couponData)
  },


  loadOrder: (id) => url.get(`/api/orders/${id}`),
  
  loadOrders: () => url.get(`/api/orders`),

  
  loadAdmin: () => url.get("/api/admin"),
}