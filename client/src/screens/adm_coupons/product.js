import React from 'react';
import { Box, Column, Button} from "rbx";

import "../../styles/restaurant.scss";


//this.checkProduct = this.checkProduct.bind(this);

function checkProduct(product, avail) 
{
    fetch(`/api/products/${product.id}`,
      {
        method: 'PUT',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*' 
        },
        
        body: JSON.stringify(
            {'availability': avail}
        )        

      }
      
    )
    //this.props.loadOrders();
    //this.forceUpdate()
   // product.availability = avail;
  }

const Product = (props) => (
  <Column size="one-third" id="product">
      <Box>
        <Column.Group gapless>
          <Column size={3} textAlign="centered">
            <img src={props.image_url} alt="new"/>
          </Column>
          <Column size={6} mobile={{size: 12}} className="infos">
            {props.availability>0 ? <h4 className="dashed_box">{props.name}</h4> : <h4 className="dashed_box_red">{props.name}</h4>} 
            {//<h4 className="subtitle has-text-weight-bold" {...props.availability>0 ? color="green" : color="red"}>
            }
            
            {//props.availability>0 ? <span className="dashed_box">Disponível</span> : <span className="dashed_box_red">Indisponível</span>
            }
          

                          <div className="div-checkbox">
<br></br>
                          <Button onClick={() => checkProduct(props, !props.availability)   }  size="medium" color="white">
                          {props.availability>0 ? <span className="dashed_box_red2">Tornar Indisponível</span> : <span className="dashed_box">Tornar disponível</span>} 
                                </Button>  
                                
      </div>
                          </Column>  
        </Column.Group>
      </Box>
  </Column>



);





export default Product;

/*

                        <Button size="medium" color="default-green">
                          <span className="has-text-white">{props.availability>0 ? "Disponível" : "Indisponível"}</span>
                          </Button>

                          */