import React, { Component, Fragment } from 'react';
import ListCoupons from '../../components/coupons'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import {Checkbox, Title, Button} from 'rbx';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { hideModal, showModal } from "../../actions/modal";

class AdmCoupons extends Component {
  state = { product: {} }

    constructor(props) {
        super(props);
        this.state = {
          coupons: []
        };
        this.loadCoupons = this.loadCoupons.bind(this);
      }



      async loadCoupons() {
        let response = await fetch(`/api/coupons`);
        const coupons = await response.json();
        this.setState({ coupons: coupons.coupons });
      }
      
      componentDidMount() {
        this.loadCoupons();
      }
      
      

      handleCheckboxChange = event =>
    this.setState({ checked: event.target.checked })

    
     

  render() {

    return (
      <Row>        
        
    <Title size={2}>Cupons</Title>
         
      <div className="div-checkbox">
      <label>
      <Checkbox className="chkbox" 
      checked={this.state.checked}
      onChange={this.handleCheckboxChange}/> 
      <b>Mostrar Finalizados</b>
      </label>
      <br/>
        <Button onClick={() => this.props.showModal('ADM_COUPON_MODAL')}>Criar cupom</Button>
            
      </div>
        <Col xs={{ span: 8, offset: 2 }} className="Orders_list">

            <p className="title">Ativos</p>
            <ListCoupons loadCoupons={this.loadCoupons} coupons={this.state.coupons.filter((coupon) => coupon.works === true)}/>
        </Col>  
    <div>
      {this.state.checked  &&
        <Fragment>
        <Col xs={{ span: 8, offset: 2 }} className="Orders_list">
              <br></br>
              <p className="title">Finalizados</p>
              <ListCoupons loadCoupons={this.loadCoupons} coupons={this.state.coupons.filter((coupon) => coupon.works === false )}/>
            </Col>
        </Fragment>
      }
    </div>
    <Fragment>
        <Col xs={{ span: 8, offset: 2 }} className="Orders_list">
              <br></br>
              </Col>
        </Fragment>     
      </Row>
    );
  }
}

const mapStateToProps = store => ({
  products: store.productsState.products,
  address: store.addressState.address
});

const mapDispatchToProps = dispatch => bindActionCreators({ hideModal, showModal }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AdmCoupons);


