import React, { Component, Fragment } from 'react';
import {Box, Column, Image, Icon, Dropdown, Button} from "rbx";
import { FaStar, FaCog, FaAngleDown } from "react-icons/fa";
import history from '../../history';

import api from "../../services/api";
import FacebookAuth from 'react-facebook-auth';
import "../../styles/restaurant.scss";

class ShowAdmin extends Component {
  state = {
    restaurant: {}
  }

  UNSAFE_componentWillMount() {
    api.getRestaurant().then(response => {
      this.setState({ restaurant: response.data.restaurant })
    });
  }

 


MyFacebookButton = ({ onClick, styles }) => 

(
  <button onClick={onClick} style={styles}>
    Login with facebook
  </button>
);

authenticate = response => {
  console.log(response);
};

  render() {
    return  <Fragment>
              <Box>
                <Column.Group>
                  <Column size={3}>
                    <Image src={this.state.restaurant.image_url} />
                  </Column>
                  <Column size={7}>
                    <p>{this.state.restaurant.description}</p>
                    <footer>
                      <span className="dashed_box">Entrega ${this.state.restaurant.delivery_tax}</span>
                      <span>
                        <Icon size="medium" color="warning">
                          <FaStar />
                        </Icon>
                        <Dropdown>
    <Dropdown.Trigger>
      <Button background-color="green">
                <Icon color="black">
                  <FaCog />
                </Icon>
                <span>Admin</span>
        <Icon size="small">
          <FaAngleDown/>
        </Icon>
      </Button>
    </Dropdown.Trigger>
    <Dropdown.Menu>
      <Dropdown.Content>
        <Dropdown.Item onClick={ () => history.push('/orders') }>Pedidos</Dropdown.Item>
        <Dropdown.Item onClick={ () => history.push('/products') }>Produtos</Dropdown.Item>
        <Dropdown.Item onClick={ () => history.push('/coupons') } >Cupons</Dropdown.Item>
        <Dropdown.Item onClick={ () => history.push('/sales') }>PDV</Dropdown.Item>
        </Dropdown.Content>
    </Dropdown.Menu>
  </Dropdown>
                        <FacebookAuth
      appId="2358315551153635"
      callback={this.authenticate}
      component={this.MyFacebookButton}
      
    />
                        <span className="has-text-warning has-text-weight-bold">Teste Irving {this.state.restaurant.review || 4.7}</span>
                      </span>
                    </footer>
                  </Column>
                </Column.Group>
              </Box>
            </Fragment>
  }
}

export default ShowAdmin;