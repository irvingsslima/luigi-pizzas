import React, { Component, Fragment } from 'react';
import ListOrders from '../../components/open_orders'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import {Checkbox, Title} from 'rbx';
import {API_WS_ROOT} from "../../constants"
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { ActionCableProvider, ActionCableConsumer} from 'react-actioncable-provider';
import { hideModal, showModal } from "../../actions/modal";
import Speech from 'speak-tts';


var message= ""

class Orders extends Component {
  state = { product: {} }

    constructor(props) {
        super(props);
        this.state = {
          orders: []
        };
        this.loadOrders = this.loadOrders.bind(this);
      }




_init(text) {
  const speech = new Speech();
  speech
    .init({
    'volume': 1,
       'lang': 'pt-BR',
       'rate': 1,
       'pitch': 1,
       'voice':'Microsoft Maria Desktop - Portuguese(Brazil)',
       'splitSentences': true,
       'listeners': {
           'onvoiceschanged': (voices) => {
               console.log("Event voiceschanged", voices)
           }
       }
})


  
speech.speak({
  
  text: `${text}`,
}).then(() => {
  console.log("Success !")
}).catch(e => {
  console.error("An error occurred :", e)
})
  
}

      async loadOrders() {
        let response = await fetch(`/api/orders`);
        const orders = await response.json();
        this.setState({ orders: orders.orders });
        this.forceUpdate();
      }
      
      componentDidMount() {
        this.loadOrders();
      }

      handleReceivedCable = response => { 
        var pizza = "", borda="Borda sem recheio;\n", drink="", meia=0, adicional=0, adicionais="Sem adicionais;\n";
        message = `Novo pedido de ${response.name}:\n`


        response.order_products.map(product => {
	if(product.product_id < 17){

		if(product.quantity < 1){
			pizza += `metade ${product.name};\n`
			meia++
		}
		else{
      pizza += `inteira ${product.name};\n`
      pizza += `${product.comment};\n`
    }

	if(meia===2){
		pizza += `${product.comment};\n`
		meia = 0;
}
  }

	if(product.product_id >  34 ){
		borda = `Pizza com borda de ${product.name}; \n`
	}

	if(product.product_id >= 19 && product.product_id < 34){
		if(adicional === 0){
    adicionais = `E adicional de ${product.name}, \n`
    }
		else{
			adicionais += `${product.name},\n`
      }
      
      adicional++;
		
	}
	
	
	if(product.product_id >= 17 && product.product_id < 19){
		drink = `E ${product.quantity} ${product.name}ITROS; \n`
	}


return {message}

})
	








message += borda + pizza + adicionais + drink;

        this._init(message);        
        console.log(message)                 
        this.props.showModal('IN_ORDER_MODAL', message)
        //alert(message)
      };

      handleCheckboxChange = event =>
    this.setState({ checked: event.target.checked })

    
     

  render() {

    return (
      <Row>        
        
  <ActionCableProvider url={API_WS_ROOT}>
  <ActionCableConsumer
          channel={{ channel: 'OrdersChannel' }}
          onReceived={this.handleReceivedCable}
        />

    <Title size={2}>Pedidos</Title>
         
      <div className="div-checkbox">
        <label>
      <Checkbox className="chkbox" 
      checked={this.state.checked}
      onChange={this.handleCheckboxChange}/> 
      <b>Mostrar Finalizados</b>
      </label>
      </div>
        <Col xs={{ span: 8, offset: 2 }} className="Orders_list">

            <p className="title">Em espera</p>
            <ListOrders loadOrders={this.loadOrders} orders={this.state.orders.filter((order) => order.status === "waiting")}/>
        </Col>  
    <div>
      {this.state.checked  &&
        <Fragment>
        <Col xs={{ span: 8, offset: 2 }} className="Orders_list">
              <br></br>
              <p className="title">Finalizados</p>
              <ListOrders loadOrders={this.loadOrders} orders={this.state.orders.filter((order) => order.status === "delivered")}/>
            </Col>
        </Fragment>
      }
    </div>
            </ActionCableProvider>
     
      </Row>
    );
  }
}

const mapStateToProps = store => ({
  products: store.productsState.products,
  address: store.addressState.address
});

const mapDispatchToProps = dispatch => bindActionCreators({ hideModal, showModal }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Orders);


