import React, { Component, Fragment } from 'react';
import { Box, Column, Image } from "rbx";
import api from "../../services/api";
import CategoryProducts from "../../components/category_products";
import "../../styles/restaurant.scss";
import MenuLine from '../../components/menu_line';
import businessHours from "business-hours.js";
import hoursJson from "../../hours.json";


class ShowRestaurant extends Component {
  state = {
    restaurant: {}
  }

  UNSAFE_componentWillMount() {
    businessHours.init(hoursJson);
    api.getRestaurant().then(response => {
      this.setState({ restaurant: response.data.restaurant })
    });
  }

  render() {
    return  <Fragment>
              <Box>
                <Column.Group>
                  <Column size={3}>
                    <Image src={this.state.restaurant.image_url} />
                  </Column>
                  <Column size={7}>
                    <p>{this.state.restaurant.description}</p>

<div>
  <br/>
  <hr/>
            {businessHours.isOpenNow() ? 
            <div>
              <span className="open">O restaurante está <span className="dashed_box">ABERTO</span></span>
              <p>
                 <span className="open">Faça seu pedido!</span>
                 </p>
                 </div>
             : 


              <div>

                
              <span className="open">Infelizmente o restaurante está <span className="dashed_box_red">FECHADO</span></span>
              <p>
                 <span className="open">Veja nossas opções e se prepare para pedir...</span>
                 </p>

                 <p>
            O restaurante abrirá:{" "}
            {businessHours.nextOpeningHour().format("DD/MM/YY HH:mm")}
          </p>
                 </div>



          
            }
            

          </div>


                    {/*
                    <footer>
                      <span className="dashed_box">Entrega ${this.state.restaurant.delivery_tax}</span>
                      <span>
                        <Icon size="medium" color="warning">
                          <FaStar />
                        </Icon>
                        <span className="has-text-warning has-text-weight-bold">{this.state.restaurant.review || 4.7}</span>
                      </span>
                    </footer>
                    */
                    }
                  </Column>
                </Column.Group>
              </Box>
              

              {this.state.restaurant.product_categories  &&
                this.state.restaurant.product_categories.map((category, i) => {
                  return <CategoryProducts key={i} restaurant={this.state.restaurant} {...category} />
                })
              }

            
              

              
<MenuLine />
            </Fragment>
            

  }
}

export default ShowRestaurant;