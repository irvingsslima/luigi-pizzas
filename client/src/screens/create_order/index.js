import React from 'react';
import OrderForm from "../../components/order_form";
import { Column, Box } from "rbx";
import "../../styles/create_order.scss"

const CreateOrder = (props) => (

  
  <Column.Group centered>   
    <Box className="shopping_cart">
        <OrderForm finish_btn_active={false}/>
      </Box>
  </Column.Group>
);

export default CreateOrder;

