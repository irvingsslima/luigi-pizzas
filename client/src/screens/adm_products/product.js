import React from 'react';
import {Field, Box, Column, Button} from "rbx";import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { hideModal, showModal } from "../../actions/modal";

import "../../styles/restaurant.scss";


//this.checkProduct = this.checkProduct.bind(this);

function checkProduct(product, avail) 
{
    fetch(`/api/products/${product.id}`,
      {
        method: 'PUT',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*' 
        },
        
        body: JSON.stringify(
            {'availability': avail}
        )        

      }
      
    )
  }

const Product = (props) => (

  <Column size="one-third" id="product">
      <Box>
        <Column.Group gapless>
          <Column size={3} textAlign="centered">
            <img src={props.image_url} alt="new"/> 
  {console.log(props)}
          </Column>
          <Column size={6} mobile={{size: 12}} className="infos">
            {props.availability>0 ? <h4 className="dashed_box">{props.name}</h4> : <h4 className="dashed_box_red">{props.name}</h4>} 



 <div className="div-more-info">
 <h4 className="dashed_box">
    {
    props.product_category.title
    
    }
    </h4>
 </div>

            {//<h4 className="subtitle has-text-weight-bold" {...props.availability>0 ? color="green" : color="red"}>
            }
            
            {//props.availability>0 ? <span className="dashed_box">Disponível</span> : <span className="dashed_box_red">Indisponível</span>
            }
          

                          <div className="div-checkbox">
<br></br>
                          <Button onClick={() => checkProduct(props, !props.availability)   }  size="medium" color="white">
                          {props.availability>0 ? <span className="dashed_box_red2">Tornar Indisponível</span> : <span className="dashed_box">Tornar disponível</span>} 
                                </Button>  
                                <Field kind="group" align="centered" id="modal-footer">
                    <Column.Group>
                    <Column>
                              <Button onClick={() =>  this.props.hideModal('ADD_PRODUCT')   }  size="medium" color="custom-orange">
                                <span className="has-text-white">Excluir</span> 
                                </Button>  
                                </Column>
                                <Column>                 
                              <Button size="medium" color="default-green" onClick={() =>  props.showModal('ADM_PRODUCT_MODAL')}>
                                <span className="has-text-white">Editar</span>
                              </Button>
                              </Column>
            
                              </Column.Group>
                          </Field>
                                
      </div>
                          </Column>  
        </Column.Group>
      </Box>
  </Column>



);





//export default Product;

const mapDispatchToProps = dispatch => bindActionCreators({ hideModal, showModal }, dispatch);

export default connect(null, mapDispatchToProps)(Product);

/*

                        <Button size="medium" color="default-green">
                          <span className="has-text-white">{props.availability>0 ? "Disponível" : "Indisponível"}</span>
                          </Button>

                          */