import React, { Component, Fragment } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import {Title, Column, Button} from 'rbx';
import SearchBox from '../../components/search_box_component';
import Product from "./product.js";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { hideModal, showModal } from "../../actions/modal";



class AdmProducts extends Component {
  state = { product: {} }

    constructor(props) {
        super(props);
        this.state = {
          orders: []
        };
        this.loadOrders = this.loadOrders.bind(this);
      }

      async loadOrders() {
        let response = await fetch(`/api/orders`);
        const orders = await response.json();
        this.setState({ orders: orders.orders });
      }
      
      componentDidMount() {
        this.loadOrders();
      }



      handleCheckboxChange = event =>
    this.setState({ checked: event.target.checked })

    
     

  render() {

    return (
      <Row>        
    <Title size={2}>Produtos</Title>
         
      <div className="div-checkbox">
        <Button onClick={() => this.props.showModal('ADM_PRODUCT_MODAL')}>Adicionar produtos</Button>
         </div>        

         <div className="div-checkbox">
         </div>     
         
    <Fragment>
        <Col xs={{ span: 8, offset: 2 }} className="Orders_list">
              <SearchBox /> 
              <br></br>              
              <Column.Group multiline gapSize={2}>
          {
            this.props.products.map((product, i) => {
              console.log(product)
              return <Product {...product} key={i}/>
            })
          }
        </Column.Group>

              </Col>
        </Fragment>
  
     
      </Row>
    );
  }
}

const mapStateToProps = store => ({
  products: store.productsState.products,
  address: store.addressState.address
});

const mapDispatchToProps = dispatch => bindActionCreators({ hideModal, showModal }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AdmProducts);


