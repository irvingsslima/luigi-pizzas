import { ADD_ORDER_ITEM } from "./action_types";
import { REMOVE_ORDER_ITEM } from "./action_types";
import { REMOVE_ALL_ORDER_ITEM } from "./action_types";
import {  REMOVE_HALF_ORDER_ITEM } from "./action_types";

export const addOrderItem = (restaurant, product, quantity, comment) => {
  return {
    type: ADD_ORDER_ITEM,
    restaurant: restaurant,
    product: product,
    quantity: quantity,
    comment: comment
  }
}

export const removeOrderItem = (orderItem) => {
  return {
    type: REMOVE_ORDER_ITEM,
    orderItem: orderItem
  }
}

export const removeHalfOrderItem = (orderItem) => {
  return {
    type: REMOVE_HALF_ORDER_ITEM,
    orderItem: orderItem
  }
}

export const removeAllOrderItem = () => {
  return {
    type: REMOVE_ALL_ORDER_ITEM,
  }
}

