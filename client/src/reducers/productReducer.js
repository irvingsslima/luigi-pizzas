import { SEARCH_PRODUCTS } from "../actions/action_types";

export default (state = { products: [] }, action) => {
  switch(action.type) {
    case SEARCH_PRODUCTS:
      return { 
        ...state,
        products: action.products
      }
    default:
      return state
  }
}