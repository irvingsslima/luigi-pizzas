import { ADD_ORDER_ITEM, REMOVE_ORDER_ITEM, REMOVE_HALF_ORDER_ITEM, REMOVE_ALL_ORDER_ITEM } from "../actions/action_types";

export default (state = { order: [] }, action) => {
  switch(action.type) {
    case ADD_ORDER_ITEM:
      let order = []

      if(state.restaurant) {
        order = state.order;
      }

      order.push({
        product: action.product,
        quantity: action.quantity,
        comment: action.comment        
      })


      return {
        ...state,
        restaurant: action.restaurant,
        order: order
    }

    case REMOVE_ORDER_ITEM:
      const indexToRemove = state.order.indexOf(action.orderItem)
      const newOrder = [...state.order];
      newOrder.splice(indexToRemove, 1);
      
      return {
        ...state,
        order: newOrder
    }

    case REMOVE_HALF_ORDER_ITEM:
      const indexToRemoveHalf = state.order.indexOf(action.orderItem)
      const newOrder2 = [...state.order];
      newOrder2.splice(indexToRemoveHalf, 1);
      newOrder2.splice((indexToRemoveHalf - 1), 1);
      
      return {
        ...state,
        order: newOrder2
    }
    

  case REMOVE_ALL_ORDER_ITEM:    
      const newCleanOrder = [];

      return {
        ...state,
        order: newCleanOrder
    }
 


    default:
      return state
  }
 
}