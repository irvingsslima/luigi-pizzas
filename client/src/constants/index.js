// src/constants/index.js

//const CNAME = 'production.imckeim3qw.us-east-2.elasticbeanstalk.com'
//const CNAME = 'localhost'
//export const API_ROOT = `http://${CNAME}`;
export const API_WS_ROOT = `wss://luigipizzas.shop/cable`;
//export const API_WS_ROOT = 'ws://localhost:3001/cable'
export const HEADERS = {
  'Content-Type': 'application/json',
  Accept: 'application/json',
};