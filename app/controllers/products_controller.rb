class ProductsController < ApplicationController
	#before_action :set_product, only: [:show, :update]

	def index
		@product = Product.all.order(:id)
		render json: @product
	end

	def show
		set_product
		render json: @product
	end

	def search
		@products = Product.search(name_or_description_cont: params[:q]).result
		#@restaurants = @restaurants.near(params[:city]) if params[:city]
		
		render json: @products
	end

	def create
		@product = Product.new(product_params)		
		if product_params[:image] && !file?(product_params[:image])
			@product.update(product_params)
		end
		if @product.save
			render json: @product, include: '**', status: :created
		else
			render json: @product.errors, status: :unprocessable_entity
		end
	end

	def update
		set_product
		@products = Product.find(params[:id])
		
		if @products.update(product_params)
		  render json: @products, status: 200
		else
		  render json: @products.errors, status: :unprocessable_entity
		end
	end

	
	def set_product
		@product = Product.find(params[:id])
		
	end

	private

	def filter_by_category
		@restaurants = @restaurants.select do |r|
			r.category.title == params[:category]
		end
	end

	def file?(param)
		param.is_a?(ActionDispatch::Http::UploadedFile)
	  end

	def product_params
		params.permit(:name, :description, :price, :availability, :image, product_category: [:id, :name] )
	end

end


