class OrdersController < ApplicationController
	before_action :set_order, only: :show
	before_action :update, only: [:edit, :update]
#render json: invoice, include: '**', status: 200
	def create
		order = Order.new(order_params)
		if order.save
			render json: order, include: '**', status: :created
			ActionCable.server.broadcast 'orders_channel', 
			OrderSerializer.new(order)
			#head :ok
		else
			render json: order.errors, status: :unprocessable_entity
		end
	end

	def show
		render json: @order
	end


	


	  def update
		@order = Order.find(params[:id])
		if @order.update(order_params)
		  render json: @order, status: 200
		else
		  render json: @order.errors, status: :unprocessable_entity
		end
	  end

	private

	def set_order
		@order = Order.find(params[:id])
	end

	
	def order_params
		params.require(:order).permit(
			:name, :phone_number, :change, :delivery_tax, :restaurant_id, :address, :status, :coupon,
			order_products_attributes: %i[quantity name comment product_id product_category]
			)
	end
end
