class Order < ApplicationRecord
	belongs_to :restaurant
	has_many :order_products

	validates :name, presence: true
	validates :phone_number, presence: true
	validates :total_value, presence: true
	validates :delivery_tax, presence: true
	#validates :coupon, presence: true
	enum status: { waiting: 0, delivered: 1 }

	accepts_nested_attributes_for :order_products,   allow_destroy: true

	before_validation :set_price

	private

	def set_price
		@final_price = 0

		#d_coupon = Coupon.find 2

		order_products.each do |order_product|
			product = Product.find order_product.product_id
			@final_price += order_product.quantity * product.price
		end
		self.total_value = @final_price + delivery_tax
		#self.total_value_discount = @final_price * d_coupon.multiplier - d_coupon.discount + delivery_tax
		#self.total_value_discount = @final_price * 0.9 + delivery_tax
	end
end
