class CouponSerializer < ActiveModel::Serializer
  attributes :id, :name, :kind, :value, :min_value, :works, :max_uses, :expiration_date
end
