class ProductCategorySerializer < ActiveModel::Serializer
  attributes :id, :title, :show

  has_many :products
end
