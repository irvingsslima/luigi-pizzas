class ProductSerializer < ActiveModel::Serializer
	include Rails.application.routes.url_helpers
	
	attributes :id, :name, :description, :price, :availability, :product_category, :image_url, :image

	def image
    return unless object.image.attached?

    object.image.blob.attributes
          .slice('filename', 'byte_size')
          .merge(url: image_url)
          .tap { |attrs| attrs['name'] = attrs.delete('filename') }
  end

  def image_url
    url_for(object.image)
  end
	
	#def image_url
	#	rails_blob_url(object.image)
	#end
end
