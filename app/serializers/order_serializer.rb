class OrderSerializer < ActiveModel::Serializer
  attributes :id, :name, :phone_number, :delivery_tax, :order_products, :address, :total_value, :change, :status
end